package team.madcat.constants;

public class Constants {
	//Application Constant Properties.
	public static final String MAIN_BGM_DIR				= "audio/main_bgm.mp3";
	public static final String GAME_BGM_DIR				= "audio/game_bgm.mp3";
	
	//SharedPreference Properties Begin.
	public static final String PREFERENCE_NAME			= "pref_bubblepang";
	public static final String FIRST_EXECUTE			= "first_execute";
	
	////Game Flag Properties.
	public static final String VIBRATION_FLAG			= "vibration";
	public static final String BGM_FLAG					= "bgm";
	public static final String SOUND_FLAG				= "sound";
	
	//SNS Developer Properties.
	public static final String FACEBOOK_APP_KEY			=	"438874212847102";

	public static final String KAKAO_STR_MSG			=	"BubblePang ����"; 
	public static final String KAKAO_STR_URL			=	"https://market.android.com/details?id=team.madcat.bubblepang";
	public static final String KAKAO_STR_INSTALL_URL	=	"https://market.android.com/details?id=team.madcat.bubblepang";
	public static final String KAKAO_STR_APPID			=	"team.madcat.bubblepang";
	public static final String KAKAO_STR_APPVER			=	"0.5";
	public static final String KAKAO_STR_APPNAME		=	"BubblePang";
	
	////Challenge Mode's Properties.
	public static final String RECORD_PLAY_TIME			= "record_play_time";
	public static final String RECORD_BEST_SCORE		= "record_best_score";
	public static final String RECORD_BLOCK_BREAK		= "record_block_break";
//	public static final String RECORD_SPEEDUP_BREAK 	= "record_speedup_break";
//	public static final String RECORD_SPEEDDW_BREAK 	= "record_speeddw_break";
//	public static final String RECORD_CLEAR_BREAK		= "record_clear_break";
//	public static final String RECORD_BLIND_BREAK		= "record_blind_break";
	//SharedPreference Properties End.
	
	//Game Trophy Properties.
	public static final String TROPHY_GAME_PLAY_1HOUR			= "trophy_game_play_1hour";
	public static final String TROPHY_GAME_PLAY_3HOUR			= "trophy_game_play_3hour";
	public static final String TROPHY_GAME_PLAY_5HOUR			= "trophy_game_play_5hour";
	
	public static final int[] TROPHY_GAME_PLAY_TIME_CRITERIA	= {60 * 60, 3 * 60 * 60, 5 * 60 * 60};
	
	public static final String TROPHY_GAME_SCORE_5000			= "trophy_game_score_5000";
	public static final String TROPHY_GAME_SCORE_10000			= "trophy_game_score_10000";
	public static final String TROPHY_GAME_SCORE_30000			= "trophy_game_score_30000";
	
	public static final int[] TROPHY_GAME_SCORE_CRITERIA		= {5000, 10000, 30000};
	
	public static final String TROPHY_REMOVE_NORMAL_500			= "trophy_remove_normal_500";
	public static final String TROPHY_REMOVE_NORMAL_1000		= "trophy_remove_normal_1000";
	public static final String TROPHY_REMOVE_NORMAL_2000		= "trophy_remove_normal_2000";
	
	public static final int[] TROPHY_REMOVE_NORMAL_CRITERIA		= {500, 1000, 2000};
	//Game Trophy Properties End.
}
