package team.madcat.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;

public class Utils {
	public static boolean is3GAvailable(Context context) {
		ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		try {
			return connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnectedOrConnecting();
		} catch (NullPointerException e) {
			return false;
		}
	}
	
	public static boolean isWifiAvailable(Context context) {
		ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		return connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnectedOrConnecting();
	}
	
	/**
	 * 부모 View를 기준으로 자식 View를 재귀 호출하면서 할당된 이미지를 해제한다.
	 * @param root
	 */
	
	public static void recursiveRecycle(View root) {
		if(root == null) {
			return;
		}
		
		if(root instanceof ViewGroup) {
			ViewGroup group = (ViewGroup)root;
			int count = group.getChildCount();
			
			for(int i=0; i < count; i++) {
				recursiveRecycle(group.getChildAt(i));
			}
			
			if(!(root instanceof AdapterView)) {
				group.removeAllViews();
			}
		}
		
		if(root instanceof ImageView) {
			((ImageView)root).setImageDrawable(null);
		}
		
		root.setBackgroundDrawable(null);
		
		root = null;
		
		return;
	}
}
