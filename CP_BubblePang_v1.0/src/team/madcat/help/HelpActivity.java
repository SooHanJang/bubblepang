package team.madcat.help;

import team.madcat.bubblepang.MainActivity;
import team.madcat.bubblepang.R;
import team.madcat.utils.Utils;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

public class HelpActivity extends Activity {
	private ImageView mHelpPageView = null;
	private ImageButton mPrevBtn = null, mNextBtn = null, mBackBtn = null;
	private int[] mHelpPages = null;
	private int mHelpPageIndex = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_layout_help);
		
		mHelpPageView = (ImageView)findViewById(R.id.help_layout_help_page);
		mPrevBtn = (ImageButton)findViewById(R.id.help_layout_prev_btn);
		mNextBtn = (ImageButton)findViewById(R.id.help_layout_next_btn);
		mBackBtn = (ImageButton)findViewById(R.id.help_layout_back_btn);
		
		mHelpPages = new int[6];
		for (int i = 0; i < 6; i++) {
			mHelpPages[i] = getResources().getIdentifier("help_page_" + i, "drawable", getPackageName());
		}
		
		mHelpPageIndex = 0;
		
		ImageButtonEventHandler imageButtonEventHandler = new ImageButtonEventHandler();
		
		mPrevBtn.setOnClickListener(imageButtonEventHandler);
		mNextBtn.setOnClickListener(imageButtonEventHandler);
		mBackBtn.setOnClickListener(imageButtonEventHandler);
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		Utils.recursiveRecycle(getWindow().getDecorView());
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
	}
	
	private class ImageButtonEventHandler implements View.OnClickListener {
		public void onClick(View v) {
			int id = v.getId();
			if (id == R.id.help_layout_prev_btn) {
				if (mHelpPageIndex > 0) {
					mHelpPageIndex--;
					mHelpPageView.setImageResource(mHelpPages[mHelpPageIndex]);
				}
			} else if (id == R.id.help_layout_next_btn) {
				if (mHelpPageIndex < 5) {
					mHelpPageIndex++;
					mHelpPageView.setImageResource(mHelpPages[mHelpPageIndex]);
				}
			} else if (id == R.id.help_layout_back_btn) {
				startActivity(new Intent(HelpActivity.this, MainActivity.class));
				finish();
			}
		}
	}
}
