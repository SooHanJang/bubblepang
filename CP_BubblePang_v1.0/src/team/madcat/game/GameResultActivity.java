package team.madcat.game;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import team.madcat.bubblepang.MainActivity;
import team.madcat.bubblepang.R;
import team.madcat.constants.Constants;
import team.madcat.facebook.FaceBookLogin;
import team.madcat.facebook.FaceBookUtil;
import team.madcat.utils.KakaoLink;
import team.madcat.utils.Utils;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class GameResultActivity extends Activity {
	public static final String GAME_RESULT = "game_result";
	public static final String GAME_REMOVE_OBJECT = "game_remove_object";
	
	private Context mContext = null;
	
	private int mCurrentGameScore = 0, mBestGameScore = 0, mCounterRemoveObject = 0, mGamePlayTime = 0;
	
	private LinearLayout mCurrentScoreLayout = null;
	private TextView mBestScoreTextView = null;
	
	private ImageButton mGotoMainBtn = null, mRestartGameBtn = null, mGotoSnsBtn = null;
	
	private boolean mGameSoundFlag = true;
	private boolean mGameGetNewTrophy = false;
	
	private SoundPool mGameResultSoundPool = null;
	private int[] mGameResultSoundValues = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_layout_game_result);
		
		mContext = this;
		
		SharedPreferences pref = getSharedPreferences(Constants.PREFERENCE_NAME, Context.MODE_PRIVATE);
		
		mCurrentGameScore = getIntent().getIntExtra(GAME_RESULT, 0);
		mCounterRemoveObject = getIntent().getIntExtra(GAME_REMOVE_OBJECT, 0);
		
		mBestGameScore = pref.getInt(Constants.RECORD_BEST_SCORE, 0);
		mGamePlayTime = pref.getInt(Constants.RECORD_PLAY_TIME, 0) + 60;
		mCounterRemoveObject = mCounterRemoveObject + pref.getInt(Constants.RECORD_BLOCK_BREAK, 0);
		
		mGameSoundFlag = pref.getBoolean(Constants.SOUND_FLAG, true);
		mGameResultSoundPool = new SoundPool(2, AudioManager.STREAM_MUSIC, 0);
		mGameResultSoundValues = new int[2];
		mGameResultSoundValues[0] = mGameResultSoundPool.load(mContext, R.raw.game_result_new_high_score, 1);
		mGameResultSoundValues[1] = mGameResultSoundPool.load(mContext, R.raw.game_result_new_trophy_get, 1);
		
		mCurrentScoreLayout = (LinearLayout)findViewById(R.id.game_result_layout_my_score_layout);
		mBestScoreTextView = (TextView)findViewById(R.id.game_result_layout_best_score_text);
		
		mGotoMainBtn = (ImageButton)findViewById(R.id.game_result_layout_main_btn);
		mRestartGameBtn = (ImageButton)findViewById(R.id.game_result_layout_restart_btn);
		mGotoSnsBtn = (ImageButton)findViewById(R.id.game_result_layout_sns_btn);
		
		ButtonEventHandler buttonEventHandler = new ButtonEventHandler();
		mGotoMainBtn.setOnClickListener(buttonEventHandler);
		mRestartGameBtn.setOnClickListener(buttonEventHandler);
		mGotoSnsBtn.setOnClickListener(buttonEventHandler);
		
		//Update Game Record
		Editor editor = pref.edit();
		
		if (mCurrentGameScore > mBestGameScore) {
			mBestGameScore = mCurrentGameScore;
			editor.putInt(Constants.RECORD_BEST_SCORE, mBestGameScore);
			
			if (mGameSoundFlag) {
				while (mGameResultSoundPool.play(mGameResultSoundValues[0], 1.0f, 1.0f, 0, 0, 1.0f) == 0) {
					SystemClock.sleep(50);
				}
			}
		}
		
		setCurrentScore();
		mBestScoreTextView.setText(Integer.toString(mBestGameScore));
		
		editor.putInt(Constants.RECORD_PLAY_TIME, mGamePlayTime);
		editor.putInt(Constants.RECORD_BLOCK_BREAK, mCounterRemoveObject);
		
		//Trophy Unlock Check
		if (Constants.TROPHY_GAME_SCORE_CRITERIA[0] <= mCurrentGameScore && mCurrentGameScore < Constants.TROPHY_GAME_SCORE_CRITERIA[1]) {
			if (!pref.getBoolean(Constants.TROPHY_GAME_SCORE_5000, false)) {
				editor.putBoolean(Constants.TROPHY_GAME_SCORE_5000, true);
				Toast.makeText(mContext, mContext.getString(R.string.toast_get_new_trophy), Toast.LENGTH_SHORT).show();
				mGameGetNewTrophy = true;
			}
		} else if (Constants.TROPHY_GAME_SCORE_CRITERIA[1] <= mCurrentGameScore && mCurrentGameScore < Constants.TROPHY_GAME_SCORE_CRITERIA[2]) {
			if (!pref.getBoolean(Constants.TROPHY_GAME_SCORE_10000, false)) {
				editor.putBoolean(Constants.TROPHY_GAME_SCORE_10000, true);
				Toast.makeText(mContext, mContext.getString(R.string.toast_get_new_trophy), Toast.LENGTH_SHORT).show();
				mGameGetNewTrophy = true;
			}
		} else if (Constants.TROPHY_GAME_SCORE_CRITERIA[2] <= mCurrentGameScore) {
			if (!pref.getBoolean(Constants.TROPHY_GAME_SCORE_30000, false)) {
				editor.putBoolean(Constants.TROPHY_GAME_SCORE_30000, true);
				Toast.makeText(mContext, mContext.getString(R.string.toast_get_new_trophy), Toast.LENGTH_SHORT).show();
				mGameGetNewTrophy = true;
			}
		}
		
		if (Constants.TROPHY_GAME_PLAY_TIME_CRITERIA[0] <= mGamePlayTime && mGamePlayTime < Constants.TROPHY_GAME_PLAY_TIME_CRITERIA[1]) {
			if (!pref.getBoolean(Constants.TROPHY_GAME_PLAY_1HOUR, false)) {
				editor.putBoolean(Constants.TROPHY_GAME_PLAY_1HOUR, true);
				Toast.makeText(mContext, mContext.getString(R.string.toast_get_new_trophy), Toast.LENGTH_SHORT).show();
				mGameGetNewTrophy = true;
			}
		} else if (Constants.TROPHY_GAME_PLAY_TIME_CRITERIA[1] <= mGamePlayTime && mGamePlayTime < Constants.TROPHY_GAME_PLAY_TIME_CRITERIA[2]) { 
			if (!pref.getBoolean(Constants.TROPHY_GAME_PLAY_3HOUR, false)) {
				editor.putBoolean(Constants.TROPHY_GAME_PLAY_3HOUR, true);
				Toast.makeText(mContext, mContext.getString(R.string.toast_get_new_trophy), Toast.LENGTH_SHORT).show();
				mGameGetNewTrophy = true;
			}
		} else if (Constants.TROPHY_GAME_PLAY_TIME_CRITERIA[2] <= mGamePlayTime) { 
			if (!pref.getBoolean(Constants.TROPHY_GAME_PLAY_5HOUR, false)) {
				editor.putBoolean(Constants.TROPHY_GAME_PLAY_5HOUR, true);
				Toast.makeText(mContext, mContext.getString(R.string.toast_get_new_trophy), Toast.LENGTH_SHORT).show();
				mGameGetNewTrophy = true;
			}
		}
		
		if (Constants.TROPHY_REMOVE_NORMAL_CRITERIA[0] <= mCounterRemoveObject && mCounterRemoveObject < Constants.TROPHY_REMOVE_NORMAL_CRITERIA[1]) {
			if (!pref.getBoolean(Constants.TROPHY_REMOVE_NORMAL_500, false)) {
				editor.putBoolean(Constants.TROPHY_REMOVE_NORMAL_500, true);
				Toast.makeText(mContext, mContext.getString(R.string.toast_get_new_trophy), Toast.LENGTH_SHORT).show();
				mGameGetNewTrophy = true;
			}
		} else if (Constants.TROPHY_REMOVE_NORMAL_CRITERIA[1] <= mCounterRemoveObject && mCounterRemoveObject < Constants.TROPHY_REMOVE_NORMAL_CRITERIA[2]) {
			if (!pref.getBoolean(Constants.TROPHY_REMOVE_NORMAL_1000, false)) {
				editor.putBoolean(Constants.TROPHY_REMOVE_NORMAL_1000, true);
				Toast.makeText(mContext, mContext.getString(R.string.toast_get_new_trophy), Toast.LENGTH_SHORT).show();
				mGameGetNewTrophy = true;
			}
		} else if (Constants.TROPHY_REMOVE_NORMAL_CRITERIA[2] <= mCounterRemoveObject) {
			if (!pref.getBoolean(Constants.TROPHY_REMOVE_NORMAL_2000, false)) {
				editor.putBoolean(Constants.TROPHY_REMOVE_NORMAL_2000, true);
				Toast.makeText(mContext, mContext.getString(R.string.toast_get_new_trophy), Toast.LENGTH_SHORT).show();
				mGameGetNewTrophy = true;
			}
		}
		
		if (mGameGetNewTrophy) {
			if (mGameResultSoundPool.play(mGameResultSoundValues[1], 1.0f, 1.0f, 0, 0, 1.0f) == 0) {
				SystemClock.sleep(50);
			}
		}
		
		//Save Record And Trophy preference.
		editor.commit();
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		Utils.recursiveRecycle(getWindow().getDecorView());
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
	}
	
	private void setCurrentScore() {
		if (mCurrentScoreLayout.getChildCount() != 0) {
			mCurrentScoreLayout.removeAllViews();
		}
		
		String currentScoreToStr = Integer.toString(mCurrentGameScore);
		
		for (int i = 0; i < currentScoreToStr.length(); i++) {
			ImageView iv = new ImageView(mContext);
			LinearLayout.LayoutParams ivLayoutParams = null;
			char number = currentScoreToStr.charAt(i);
			
			if (number == '0') {
				ivLayoutParams = new LinearLayout.LayoutParams(44, 59);
				iv.setImageResource(R.drawable.game_number_0);
			} else if (number == '1') {
				ivLayoutParams = new LinearLayout.LayoutParams(22, 59);
				iv.setImageResource(R.drawable.game_number_1);
			} else if (number == '2') {
				ivLayoutParams = new LinearLayout.LayoutParams(44, 59);
				iv.setImageResource(R.drawable.game_number_2);
			} else if (number == '3') {
				ivLayoutParams = new LinearLayout.LayoutParams(37, 59);
				iv.setImageResource(R.drawable.game_number_3);
			} else if (number == '4') {
				ivLayoutParams = new LinearLayout.LayoutParams(44, 59);
				iv.setImageResource(R.drawable.game_number_4);
			} else if (number == '5') {
				ivLayoutParams = new LinearLayout.LayoutParams(41, 59);
				iv.setImageResource(R.drawable.game_number_5);
			} else if (number == '6') {
				ivLayoutParams = new LinearLayout.LayoutParams(44, 59);
				iv.setImageResource(R.drawable.game_number_6);
			} else if (number == '7') {
				ivLayoutParams = new LinearLayout.LayoutParams(44, 59);
				iv.setImageResource(R.drawable.game_number_7);
			} else if (number == '8') {
				ivLayoutParams = new LinearLayout.LayoutParams(44, 59);
				iv.setImageResource(R.drawable.game_number_8);
			} else if (number == '9') {
				ivLayoutParams = new LinearLayout.LayoutParams(44, 59);
				iv.setImageResource(R.drawable.game_number_9);
			}
			
			iv.setScaleType(ScaleType.FIT_CENTER);
			mCurrentScoreLayout.addView(iv, ivLayoutParams);
		}
	}
	
	private void showSnsDialog() {
		new SnsPostDialog(mContext).show();
	}
	
	private class SnsPostDialog extends Dialog {
		private ImageButton kakaoTalkBtn = null, facebookBtn = null, closeBtn = null;
		
		public SnsPostDialog(Context context) {
			// TODO Auto-generated constructor stub
			super(context);
		}
		
		@Override
		protected void onCreate(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onCreate(savedInstanceState);
			
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			setContentView(R.layout.dialog_layout_sns);
			getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			
			kakaoTalkBtn = (ImageButton)findViewById(R.id.sns_layout_kakao_btn);
			facebookBtn = (ImageButton)findViewById(R.id.sns_layout_facebook_btn);
			closeBtn = (ImageButton)findViewById(R.id.sns_layout_dismiss_btn);
			
			ClickEventHandler clickEventHandler = new ClickEventHandler();
			kakaoTalkBtn.setOnClickListener(clickEventHandler);
			facebookBtn.setOnClickListener(clickEventHandler);
			closeBtn.setOnClickListener(clickEventHandler);
			
			setCanceledOnTouchOutside(false);
		}
		
		@Override
		public void onBackPressed() {
			// TODO Auto-generated method stub
		}
		
		private class ClickEventHandler implements View.OnClickListener {
			public void onClick(View v) {
				int id = v.getId();
				if (id == R.id.sns_layout_kakao_btn) {
					if (Utils.is3GAvailable(mContext) || Utils.isWifiAvailable(mContext)) {
						Intent kakaoTalkSendIntent = new Intent(Intent.ACTION_SEND);
						kakaoTalkSendIntent.setType("text/plain");
						
						PackageManager packageManager = getPackageManager();
						List<ResolveInfo> resolveInfos = packageManager.queryIntentActivities(kakaoTalkSendIntent, 0);
						boolean isKakaoExist = false;
						
						for (int i = 0; i < resolveInfos.size(); i++) {
							if (resolveInfos.get(i).activityInfo.packageName.toLowerCase().equals("com.kakao.talk")) {
								isKakaoExist = true;
								break;
							}
						}
						
						if (isKakaoExist) {
							ArrayList<Map<String, String>> arrMetaInfo = new ArrayList<Map<String,String>>();
									
							Map<String, String> metaInfoAndroid = new Hashtable<String, String>(1);
							metaInfoAndroid.put("os", "android");
							metaInfoAndroid.put("devicetype", "phone");
							metaInfoAndroid.put("installurl", Constants.KAKAO_STR_INSTALL_URL);
					        metaInfoAndroid.put("executeurl", "etoile://");
					        arrMetaInfo.add(metaInfoAndroid);
							
					        String sendMsg = mContext.getString(R.string.sns_my_bubblepang_score) + mCurrentGameScore;
					        
							try {
								KakaoLink link = new KakaoLink(mContext, Constants.KAKAO_STR_URL, Constants.KAKAO_STR_APPID, Constants.KAKAO_STR_APPVER, sendMsg, Constants.KAKAO_STR_APPNAME, arrMetaInfo, "UTF-8");
								startActivity(link.getIntent());
							} catch (UnsupportedEncodingException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								Toast.makeText(mContext, mContext.getString(R.string.toast_kakao_execute_fail), Toast.LENGTH_SHORT).show();
							}
						} else {
							Toast.makeText(mContext, mContext.getString(R.string.toast_kakao_not_exist), Toast.LENGTH_SHORT).show();
						}
					} else {
						Toast.makeText(mContext, mContext.getString(R.string.toast_network_not_working), Toast.LENGTH_SHORT).show();
					}
					dismiss();
				} else if (id == R.id.sns_layout_facebook_btn) {
					if (Utils.is3GAvailable(mContext) || Utils.isWifiAvailable(mContext)) {
						if (Constants.FACEBOOK_APP_KEY == null) {
				            FaceBookUtil.showAlert(getApplicationContext(), "Warning", "Facebook Applicaton ID must be specified before running this example: see Example.java");
						}
						
			            FaceBookLogin faceBookLogin = new FaceBookLogin(mContext, GameResultActivity.this, mContext.getString(R.string.sns_my_bubblepang_score) + mCurrentGameScore);
						faceBookLogin.login();
					} else {
						Toast.makeText(mContext, mContext.getString(R.string.toast_network_not_working), Toast.LENGTH_SHORT).show();
					}
					dismiss();
				} else if (id == R.id.sns_layout_dismiss_btn) {
					dismiss();
				}
			}
		}
	}
	
	private class ButtonEventHandler implements View.OnClickListener {
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Intent intent = null;
			
			int id = v.getId();
			if (id == R.id.game_result_layout_main_btn) {
				intent = new Intent(GameResultActivity.this, MainActivity.class);
			} else if (id == R.id.game_result_layout_restart_btn) {
				intent = new Intent(GameResultActivity.this, GameActivity.class);
			} else if (id == R.id.game_result_layout_sns_btn) {
				showSnsDialog();
			}
			
			if (intent != null) {
				startActivity(intent);
				finish();
			}
		}
	}
}
