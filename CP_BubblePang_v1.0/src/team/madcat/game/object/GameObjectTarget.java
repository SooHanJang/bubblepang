package team.madcat.game.object;

import java.util.Random;

public class GameObjectTarget {
	private double mFrameSpeed = 0;
	private double mInitAngle = 0;
	private float mStartOffset = 0;
	
	private double mX = 0, mY = 0, mXSpeed = 0, mYSpeed = 0;
	private int mDirection = 0, mObjectType = 0;
	
	private float mScreenWidthWeight = 0, mScreenHeightWeight = 0;
	
	private boolean mAnimationFlag = false;
	private int mRemaineAnimationTime = 0;
	
	private float mInitSpeedCorrectionValue = 1.0f;
	
	public GameObjectTarget(long time, float screenWidth, float screenHeight) {
		// TODO Auto-generated constructor stub
		mScreenWidthWeight = screenWidth / GameObjectConstants.DEFAULT_SCREEN_WIDTH;
		mScreenHeightWeight = screenHeight / GameObjectConstants.DEFAULT_SCREEN_HEIGHT;
		
		if (mScreenWidthWeight <= 1 ||  mScreenHeightWeight <= 1) {
			mInitSpeedCorrectionValue = 1.0f;
		} else if ((1 < mScreenWidthWeight && mScreenWidthWeight <= 1.2) || (1 < mScreenHeightWeight && mScreenHeightWeight <= 1.2)) {
			mInitSpeedCorrectionValue = 1.1f;
		} else if ((1.2 < mScreenWidthWeight && mScreenWidthWeight <= 1.4) || (1.2 < mScreenHeightWeight && mScreenHeightWeight <= 1.4)) {
			mInitSpeedCorrectionValue = 1.2f;
		} else if ((1.4 < mScreenWidthWeight && mScreenWidthWeight <= 1.6) || (1.4 < mScreenHeightWeight && mScreenHeightWeight <= 1.6)) {
			mInitSpeedCorrectionValue = 1.3f;
		} else if ((1.6 < mScreenWidthWeight && mScreenWidthWeight <= 1.8) || (1.6 < mScreenHeightWeight && mScreenHeightWeight <= 1.8)){
			mInitSpeedCorrectionValue = 1.4f;
		} else {
			mInitSpeedCorrectionValue = 1.5f;
		}
		
		Random random = new Random();
		int randomValue = random.nextInt(2);
		
		switch (randomValue) {
		case GameObjectConstants.MODE_VERTICAL:
			mInitAngle = random.nextDouble() * (GameObjectConstants.MAX_VERTICAL_ANGLE - GameObjectConstants.MIN_VERTICAL_ANGLE) + GameObjectConstants.MIN_VERTICAL_ANGLE;
			
			mFrameSpeed = (random.nextDouble() * (GameObjectConstants.MAX_VERTICAL_SPEED - GameObjectConstants.MIN_VERTICAL_SPEED) + GameObjectConstants.MIN_VERTICAL_SPEED) * mScreenWidthWeight;
			mXSpeed = (GameObjectConstants.INIT_SPEED * mInitSpeedCorrectionValue) * Math.cos(Math.toRadians(mInitAngle));
			mYSpeed = (GameObjectConstants.INIT_SPEED * mInitSpeedCorrectionValue) * Math.sin(Math.toRadians(mInitAngle)) * -1;
			
			mStartOffset = (random.nextInt(GameObjectConstants.OFFSET_VERTICAL) - GameObjectConstants.OFFSET_VERTICAL / 2) * mScreenWidthWeight;
			
			mX = screenWidth / 2 + mStartOffset;
			mY = screenHeight;
			
			randomValue = random.nextInt(2);
			if (randomValue == 0) {
				mDirection = -1;
			} else {
				mDirection = 1;
			}
			break;
		case GameObjectConstants.MODE_HORIZONTAL:
			mInitAngle = random.nextDouble() * (GameObjectConstants.MAX_HORIZONTAL_ANGLE - GameObjectConstants.MIN_HORIZONTAL_ANGLE) + GameObjectConstants.MIN_HORIZONTAL_ANGLE;
			
			mFrameSpeed = (random.nextDouble() * (GameObjectConstants.MAX_HORIZONTAL_SPEED - GameObjectConstants.MIN_HORIZONTAL_SPEED) + GameObjectConstants.MIN_HORIZONTAL_SPEED) * mScreenHeightWeight;
			mXSpeed = (GameObjectConstants.INIT_SPEED * mInitSpeedCorrectionValue) * Math.cos(Math.toRadians(mInitAngle));
			mYSpeed = (GameObjectConstants.INIT_SPEED * mInitSpeedCorrectionValue) * Math.sin(Math.toRadians(mInitAngle)) * -1;
			
			mStartOffset = (random.nextInt(GameObjectConstants.OFFSET_HORIZONTAL) - GameObjectConstants.OFFSET_HORIZONTAL / 2) * mScreenHeightWeight;
			
			randomValue = random.nextInt(2);
			if (randomValue == 0) {
				mDirection = -1;
				mX = screenWidth;
			} else {
				mDirection = 1;
				mX = 0;
			}
			mY = screenHeight / 2 + mStartOffset;
			
			break;
		}
		
		//test code begin
		mObjectType = GameObjectConstants.TYPE_NORMAL_HUGE;
		//test code end
		
		randomValue = random.nextInt(100);
		
		if (time < 10000) {
			if (0 <= randomValue && randomValue < 63) {
				mObjectType = GameObjectConstants.TYPE_NORMAL_HUGE;
			} else if (63 <= randomValue && randomValue < 81) {
				mObjectType = GameObjectConstants.TYPE_NORMAL_MIDI;
			} else if (81 <= randomValue && randomValue < 90) {
				mObjectType = GameObjectConstants.TYPE_NORMAL_TINY;
			} else if (90 <= randomValue && randomValue < 93) {
				mObjectType = GameObjectConstants.TYPE_POSITIVE_SPEED;
			} else if (93 <= randomValue && randomValue < 95) {
				mObjectType = GameObjectConstants.TYPE_POSITIVE_CLEAR;
			} else if (95 <= randomValue && randomValue < 98) {
				mObjectType = GameObjectConstants.TYPE_NEGATIVE_SPEED;
			} else if (98 <= randomValue && randomValue < 100) {
				mObjectType = GameObjectConstants.TYPE_NEGATIVE_BLIND;
			} else {
				//Exceptions
				mObjectType = GameObjectConstants.TYPE_NORMAL_HUGE;
			}
		} else if (time < 20000) {
			if (0 <= randomValue && randomValue < 54) {
				mObjectType = GameObjectConstants.TYPE_NORMAL_HUGE;
			} else if (54 <= randomValue && randomValue < 81) {
				mObjectType = GameObjectConstants.TYPE_NORMAL_MIDI;
			} else if (81 <= randomValue && randomValue < 90) {
				mObjectType = GameObjectConstants.TYPE_NORMAL_TINY;
			} else if (90 <= randomValue && randomValue < 93) {
				mObjectType = GameObjectConstants.TYPE_POSITIVE_SPEED;
			} else if (93 <= randomValue && randomValue < 95) {
				mObjectType = GameObjectConstants.TYPE_POSITIVE_CLEAR;
			} else if (95 <= randomValue && randomValue < 98) {
				mObjectType = GameObjectConstants.TYPE_NEGATIVE_SPEED;
			} else if (98 <= randomValue && randomValue < 100) {
				mObjectType = GameObjectConstants.TYPE_NEGATIVE_BLIND;
			} else {
				//Exceptions
				mObjectType = GameObjectConstants.TYPE_NORMAL_HUGE;
			}
		} else if (time < 30000) {
			if (0 <= randomValue && randomValue < 45) {
				mObjectType = GameObjectConstants.TYPE_NORMAL_HUGE;
			} else if (45 <= randomValue && randomValue < 63) {
				mObjectType = GameObjectConstants.TYPE_NORMAL_MIDI;
			} else if (63 <= randomValue && randomValue < 90) {
				mObjectType = GameObjectConstants.TYPE_NORMAL_TINY;
			} else if (90 <= randomValue && randomValue < 93) {
				mObjectType = GameObjectConstants.TYPE_POSITIVE_SPEED;
			} else if (93 <= randomValue && randomValue < 95) {
				mObjectType = GameObjectConstants.TYPE_POSITIVE_CLEAR;
			} else if (95 <= randomValue && randomValue < 98) {
				mObjectType = GameObjectConstants.TYPE_NEGATIVE_SPEED;
			} else if (98 <= randomValue && randomValue < 100) {
				mObjectType = GameObjectConstants.TYPE_NEGATIVE_BLIND;
			} else {
				//Exceptions
				mObjectType = GameObjectConstants.TYPE_NORMAL_HUGE;
			}
		} else if (time < 40000) {
			if (0 <= randomValue && randomValue < 36) {
				mObjectType = GameObjectConstants.TYPE_NORMAL_HUGE;
			} else if (36 <= randomValue && randomValue < 63) {
				mObjectType = GameObjectConstants.TYPE_NORMAL_MIDI;
			} else if (63 <= randomValue && randomValue < 90) {
				mObjectType = GameObjectConstants.TYPE_NORMAL_TINY;
			} else if (90 <= randomValue && randomValue < 93) {
				mObjectType = GameObjectConstants.TYPE_POSITIVE_SPEED;
			} else if (93 <= randomValue && randomValue < 95) {
				mObjectType = GameObjectConstants.TYPE_POSITIVE_CLEAR;
			} else if (95 <= randomValue && randomValue < 98) {
				mObjectType = GameObjectConstants.TYPE_NEGATIVE_SPEED;
			} else if (98 <= randomValue && randomValue < 100) {
				mObjectType = GameObjectConstants.TYPE_NEGATIVE_BLIND;
			} else {
				//Exceptions
				mObjectType = GameObjectConstants.TYPE_NORMAL_HUGE;
			}
		} else if (time < 50000) {
			if (0 <= randomValue && randomValue < 27) {
				mObjectType = GameObjectConstants.TYPE_NORMAL_HUGE;
			} else if (27 <= randomValue && randomValue < 45) {
				mObjectType = GameObjectConstants.TYPE_NORMAL_MIDI;
			} else if (45 <= randomValue && randomValue < 90) {
				mObjectType = GameObjectConstants.TYPE_NORMAL_TINY;
			} else if (90 <= randomValue && randomValue < 93) {
				mObjectType = GameObjectConstants.TYPE_POSITIVE_SPEED;
			} else if (93 <= randomValue && randomValue < 95) {
				mObjectType = GameObjectConstants.TYPE_POSITIVE_CLEAR;
			} else if (95 <= randomValue && randomValue < 98) {
				mObjectType = GameObjectConstants.TYPE_NEGATIVE_SPEED;
			} else if (98 <= randomValue && randomValue < 100) {
				mObjectType = GameObjectConstants.TYPE_NEGATIVE_BLIND;
			} else {
				//Exceptions
				mObjectType = GameObjectConstants.TYPE_NORMAL_HUGE;
			}
		} else if (time <= 60000) {
			if (0 <= randomValue && randomValue < 18) {
				mObjectType = GameObjectConstants.TYPE_NORMAL_HUGE;
			} else if (18 <= randomValue && randomValue < 45) {
				mObjectType = GameObjectConstants.TYPE_NORMAL_MIDI;
			} else if (45 <= randomValue && randomValue < 90) {
				mObjectType = GameObjectConstants.TYPE_NORMAL_TINY;
			} else if (90 <= randomValue && randomValue < 93) {
				mObjectType = GameObjectConstants.TYPE_POSITIVE_SPEED;
			} else if (93 <= randomValue && randomValue < 95) {
				mObjectType = GameObjectConstants.TYPE_POSITIVE_CLEAR;
			} else if (95 <= randomValue && randomValue < 98) {
				mObjectType = GameObjectConstants.TYPE_NEGATIVE_SPEED;
			} else if (98 <= randomValue && randomValue < 100) {
				mObjectType = GameObjectConstants.TYPE_NEGATIVE_BLIND;
			} else {
				//Exceptions
				mObjectType = GameObjectConstants.TYPE_NORMAL_HUGE;
			}
		}
		
		switch (mObjectType) {
		case GameObjectConstants.TYPE_NORMAL_HUGE:
		case GameObjectConstants.TYPE_NORMAL_MIDI:
		case GameObjectConstants.TYPE_NORMAL_TINY:
			mRemaineAnimationTime = GameObjectConstants.ANIMATION_TIME_NORMAL;
			break;
		case GameObjectConstants.TYPE_POSITIVE_SPEED:
			mRemaineAnimationTime = GameObjectConstants.ANIMATION_TIME_SPEED_DW;
			break;
		case GameObjectConstants.TYPE_POSITIVE_CLEAR:
			mRemaineAnimationTime = GameObjectConstants.ANIMATION_TIME_CLEAR;
			break;
		case GameObjectConstants.TYPE_NEGATIVE_SPEED:
			mRemaineAnimationTime = GameObjectConstants.ANIMATION_TIME_SPEED_UP;
			break;
		case GameObjectConstants.TYPE_NEGATIVE_BLIND:
			mRemaineAnimationTime = GameObjectConstants.ANIMATION_TIME_BLIND;
			break;
		}
	}

	public static final int SPEED_UP = 0;	//speed up
	public static final int SPEED_DW = 1;	//speed down
	
	public void moveObject(int control) {
		if (!mAnimationFlag) {
			switch (control) {
			case SPEED_UP:
				mX = mX + (mDirection * mXSpeed * (mFrameSpeed * GameObjectConstants.SPEED_CONTROL_WEIGHT));
				
				mYSpeed = mYSpeed + (mFrameSpeed * GameObjectConstants.SPEED_CONTROL_WEIGHT);
				mY = mY + mYSpeed * (mFrameSpeed * GameObjectConstants.SPEED_CONTROL_WEIGHT);
				break;
			case SPEED_DW:
				mX = mX + (mDirection * mXSpeed * (mFrameSpeed / GameObjectConstants.SPEED_CONTROL_WEIGHT));
				
				mYSpeed = mYSpeed + (mFrameSpeed / GameObjectConstants.SPEED_CONTROL_WEIGHT);
				mY = mY + mYSpeed * (mFrameSpeed / GameObjectConstants.SPEED_CONTROL_WEIGHT);
				break;
			}
		}
	}
	
	public void moveObject() {
		if (!mAnimationFlag) {
			mX = mX + (mDirection * mXSpeed * mFrameSpeed);
			
			mYSpeed = mYSpeed + mFrameSpeed;
			mY = mY + mYSpeed * mFrameSpeed;
		}
	}
	
	public double getX() { return mX; }
	public double getY() { return mY; }
	
	public float getWidth() {
		// TODO Auto-generated method stub
		switch (mObjectType) {
		case GameObjectConstants.TYPE_NORMAL_TINY:
			return GameObjectConstants.SIZE_OBJECT_TINY * mScreenWidthWeight;
		case GameObjectConstants.TYPE_NORMAL_MIDI:
			return GameObjectConstants.SIZE_OBJECT_MIDI * mScreenWidthWeight;
		case GameObjectConstants.TYPE_NORMAL_HUGE:
			return GameObjectConstants.SIZE_OBJECT_HUGE * mScreenWidthWeight;
		case GameObjectConstants.TYPE_NEGATIVE_BLIND:
			if (mAnimationFlag) {
				return GameObjectConstants.SIZE_OBJECT_BLIND * mScreenWidthWeight;
			} else {
				return GameObjectConstants.SIZE_OBJECT_MIDI * mScreenWidthWeight;
			}
		default:
			return GameObjectConstants.SIZE_OBJECT_MIDI * mScreenWidthWeight;
		}
	}

	public float getHeight() {
		// TODO Auto-generated method stub
		switch (mObjectType) {
		case GameObjectConstants.TYPE_NORMAL_TINY:
			return GameObjectConstants.SIZE_OBJECT_TINY * mScreenHeightWeight;
		case GameObjectConstants.TYPE_NORMAL_MIDI:
			return GameObjectConstants.SIZE_OBJECT_MIDI * mScreenHeightWeight;
		case GameObjectConstants.TYPE_NORMAL_HUGE:
			return GameObjectConstants.SIZE_OBJECT_HUGE * mScreenHeightWeight;
		case GameObjectConstants.TYPE_NEGATIVE_BLIND:
			if (mAnimationFlag) {
				return GameObjectConstants.SIZE_OBJECT_BLIND * mScreenHeightWeight;
			} else {
				return GameObjectConstants.SIZE_OBJECT_MIDI * mScreenHeightWeight;
			}
		default:
			return GameObjectConstants.SIZE_OBJECT_MIDI * mScreenHeightWeight;
		}
	}
	
	public int getObjectType() { return mObjectType; }
	
	public int getObjectScore() {
		// TODO Auto-generated method stub
		switch (mObjectType) {
		case GameObjectConstants.TYPE_NORMAL_TINY:
			return GameObjectConstants.SCORE_OBJECT_TINY;
		case GameObjectConstants.TYPE_NORMAL_MIDI:
			return GameObjectConstants.SCORE_OBJECT_MIDI;
		case GameObjectConstants.TYPE_NORMAL_HUGE:
			return GameObjectConstants.SCORE_OBJECT_HUGE;
		}
		
		return 0;
	}
	
	public void setAnimationFlag(boolean flag) {
		mAnimationFlag = flag;
	}
	
	public boolean getAnimationFlag() {
		return mAnimationFlag;
	}
	
	public void setRemainAnimationTime(int remainTime) {
		mRemaineAnimationTime = remainTime;
	}
	
	public void decreaseAnimationTime(int decreaseTime) {
		if (mRemaineAnimationTime != 0) {
			mRemaineAnimationTime = mRemaineAnimationTime - decreaseTime;
		}
	}
	
	public int getRemainAnimationTime() {
		return mRemaineAnimationTime;
	}
	
	public boolean isItemTargetObject() {
		switch (mObjectType) {
		case GameObjectConstants.TYPE_NORMAL_HUGE:
		case GameObjectConstants.TYPE_NORMAL_MIDI:
		case GameObjectConstants.TYPE_NORMAL_TINY:
			return false;
		case GameObjectConstants.TYPE_POSITIVE_SPEED:
		case GameObjectConstants.TYPE_POSITIVE_CLEAR:
		case GameObjectConstants.TYPE_NEGATIVE_SPEED:
		case GameObjectConstants.TYPE_NEGATIVE_BLIND:
			return true;
		}
		
		return false;
	}
}
