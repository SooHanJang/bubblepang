package team.madcat.game.object;

public class GameObjectConstants {
	public static final int DEFAULT_SCREEN_WIDTH	= 800;
	public static final int DEFAULT_SCREEN_HEIGHT	= 480;
	
	public static final int MODE_VERTICAL			= 0;
	public static final int MODE_HORIZONTAL			= 1;
	
	public static final double INIT_SPEED			= 29;
	
	public static final float MIN_VERTICAL_SPEED	= 0.7f;
	public static final float MAX_VERTICAL_SPEED	= 1.2f;
	
	public static final float MIN_HORIZONTAL_SPEED	= 0.5f;
	public static final float MAX_HORIZONTAL_SPEED	= 0.8f;
	
//	public static final float MIN_SPEED_LV1		= 0.5f;
//	public static final float MAX_SPEED_LV1		= 1.4f;
//	public static final float MIN_SPEED_LV2		= 1.5f;
//	public static final float MAX_SPEED_LV2		= 2.4f;
//	public static final float MIN_SPEED_LV3		= 2.5f;
//	public static final float MAX_SPEED_LV3		= 3.4f;
//	public static final float MIN_SPEED_LV4		= 3.5f;
//	public static final float MAX_SPEED_LV4		= 4.4f;
//	public static final float MIN_SPEED_LV5		= 4.5f;
//	public static final float MAX_SPEED_LV5		= 5.4f;
	
	public static final int MIN_VERTICAL_ANGLE		= 75;
	public static final int MAX_VERTICAL_ANGLE		= 85;
	
	public static final int MIN_HORIZONTAL_ANGLE	= 0;
	public static final int MAX_HORIZONTAL_ANGLE	= 40;
	
	public static final int OFFSET_VERTICAL			= 200;
	public static final int OFFSET_HORIZONTAL		= 100;
	
	public static final int DIRECTION_RL			= -1;
	public static final int DIRECTION_LR			= 1;

	public static final int TYPE_NORMAL_TINY		= 0;
	public static final int TYPE_NORMAL_MIDI		= 1;
	public static final int TYPE_NORMAL_HUGE		= 2;
	
	public static final int TYPE_POSITIVE_SPEED		= 3;
	public static final int TYPE_POSITIVE_CLEAR		= 4;
	
	public static final int TYPE_NEGATIVE_SPEED		= 5;
	public static final int TYPE_NEGATIVE_BLIND		= 6;
	
	public static final float SIZE_OBJECT_TINY		= 30 * 2;
	public static final float SIZE_OBJECT_MIDI		= 40 * 2;
	public static final float SIZE_OBJECT_HUGE		= 50 * 2;
	
	public static final float SIZE_OBJECT_BLIND		= 200 * 2;
	
	public static final int SCORE_OBJECT_TINY		= 50;
	public static final int SCORE_OBJECT_MIDI		= 30;
	public static final int SCORE_OBJECT_HUGE		= 10;
	
	public static final float SPEED_CONTROL_WEIGHT	= 1.5f;
	
	public static final int ANIMATION_TIME_NORMAL	= 750;
	public static final int ANIMATION_TIME_SPEED_UP	= 500;
	public static final int ANIMATION_TIME_SPEED_DW	= 500;
	public static final int ANIMATION_TIME_BLIND	= 3500;
	public static final int ANIMATION_TIME_CLEAR	= 500;
}
