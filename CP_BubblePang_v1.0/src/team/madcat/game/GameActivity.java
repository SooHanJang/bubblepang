package team.madcat.game;

import team.madcat.audio.GameAudioPlayer;
import team.madcat.bubblepang.MainActivity;
import team.madcat.bubblepang.R;
import team.madcat.constants.Constants;
import team.madcat.utils.Utils;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class GameActivity extends Activity {
	private Context mContext = null;
	
	private RelativeLayout mMainLayout = null, mUpperBarLayout = null;
	private ImageButton mPauseBtn = null;

	private LinearLayout mScoreLayout = null;
	private ImageView[] mTimeImageView = null;
	
	private GameSurfaceView mSurfaceView = null;
	private GameAudioPlayer mGameAudioPlayer = null;
	private SoundPool mGameEffectSoundPool = null;
	private int mGameStartEffectSound = 0;
	private int mGameEndEffectSound = 0;
	
	private boolean mVibrationFlag = true;
	private boolean mBgmFlag = true;
	private boolean mSoundFlag = true;
	
	private boolean mIsDialogShow = false;
	private boolean mActivityFinishFlag = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		mContext = this;
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		setContentView(R.layout.activity_layout_game);
		
		mMainLayout = (RelativeLayout)findViewById(R.id.game_layout_main_layout);
		mUpperBarLayout = (RelativeLayout)findViewById(R.id.game_layout_upperbar_layout);
		
		mPauseBtn = (ImageButton)findViewById(R.id.game_layout_pause_btn);
		mScoreLayout = (LinearLayout)findViewById(R.id.game_layout_score_layout);
		mTimeImageView = new ImageView[2];
		mTimeImageView[0] = (ImageView)findViewById(R.id.game_layout_time_0);
		mTimeImageView[1] = (ImageView)findViewById(R.id.game_layout_time_1);
		
		mSurfaceView = new GameSurfaceView(mContext);
		mGameAudioPlayer = new GameAudioPlayer(mContext);
		mGameEffectSoundPool = new SoundPool(2, AudioManager.STREAM_MUSIC, 0);
		mGameStartEffectSound = mGameEffectSoundPool.load(getBaseContext(), R.raw.game_play_start, 1);
		mGameEndEffectSound = mGameEffectSoundPool.load(getBaseContext(), R.raw.game_play_end, 1);
		
		SharedPreferences pref = getSharedPreferences(Constants.PREFERENCE_NAME, Context.MODE_PRIVATE);
		mVibrationFlag = pref.getBoolean(Constants.VIBRATION_FLAG, true);
		mBgmFlag = pref.getBoolean(Constants.BGM_FLAG, true);
		mSoundFlag = pref.getBoolean(Constants.SOUND_FLAG, true);
		
		mPauseBtn.setOnClickListener(new ButtonEventHandler());
		
		mSurfaceView.setVibrateFlag(mVibrationFlag);
		mSurfaceView.setSoundFlag(mSoundFlag);
		mSurfaceView.startGameEngine((GameActivity)mContext);
		
		if (mBgmFlag) {
			mGameAudioPlayer.startBGM(R.raw.game_bgm, true);
		}
		
		mMainLayout.addView(mSurfaceView, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT, RelativeLayout.LayoutParams.FILL_PARENT));
		mMainLayout.bringChildToFront(mUpperBarLayout);
		
		mIsDialogShow = false;
		mActivityFinishFlag = false;
		
		setScore(0);
		setTime(60);
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		
		if (mActivityFinishFlag) {
			finish();
		} else {
			showPauseDialog();
		}
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		mSurfaceView.stopGameEngine();
		mGameAudioPlayer.stopBGM();
		Utils.recursiveRecycle(getWindow().getDecorView());
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
	}
	
	public void setScore(int gameScore) {
		String gameScoreStr = Integer.toString(gameScore);
		
		if (mScoreLayout.getChildCount() != 0) {
			mScoreLayout.removeAllViews();
		}
		
		for (int i = 0; i < gameScoreStr.length(); i++) {
			ImageView iv = new ImageView(mContext);
			LinearLayout.LayoutParams ivLayoutParams = new LinearLayout.LayoutParams(30, 45);
			iv.setImageResource(getResources().getIdentifier("game_score_" + gameScoreStr.charAt(i), "drawable", getPackageName()));
			mScoreLayout.addView(iv, ivLayoutParams);
		}
	}
	
	public void setTime(long time) {
		setTime((int)time);
	}
	
	public void setTime(int time) {
		String timeStr = Integer.toString(time);
		
		if (timeStr.length() == 1) {
			mTimeImageView[0].setImageResource(R.drawable.game_time_0);
			mTimeImageView[1].setImageResource(getResources().getIdentifier("game_time_" + timeStr, "drawable", getPackageName()));
		} else {
			for (int i = 0; i < timeStr.length(); i++) {
				mTimeImageView[i].setImageResource(getResources().getIdentifier("game_time_" + timeStr.charAt(i), "drawable", getPackageName()));
			}
		}
	}
	
	public void setEnableMenuBtn(boolean enabled) {
		mPauseBtn.setEnabled(enabled);
	}
	
	public void playGameStartSound() {
		if (mSoundFlag) {
			while (mGameEffectSoundPool.play(mGameStartEffectSound, 2.0f, 2.0f, 0, 0, 1.0f) == 0) {
				SystemClock.sleep(50);
			}
		}
	}
	
	public void playGameOverSound() {
		if (mSoundFlag) {
			while (mGameEffectSoundPool.play(mGameEndEffectSound, 2.0f, 2.0f, 0, 0, 1.0f) == 0) {
				SystemClock.sleep(50);
			}
		}
	}
	
	public void showPauseDialog() {
		if (!mIsDialogShow) {
			mSurfaceView.pauseGameEngine();
			
			if (mBgmFlag) {
				mGameAudioPlayer.pauseBGM();
			}
			
			new PauseDialog(mContext).show();
			mIsDialogShow = true;
		}
	}
	
	public void showTimeOverDialog(int gameScore, int gameRemoveObject) {
		if (!mIsDialogShow) {
			mSurfaceView.pauseGameEngine();
			
			if (mBgmFlag) { mGameAudioPlayer.stopBGM(); }
			playGameOverSound();
			
			new TimeOverDialog(mContext, gameScore, gameRemoveObject).show();
			mIsDialogShow = true;
		}
	}
	
	private class ButtonEventHandler implements View.OnClickListener {
		public void onClick(View v) {
			if (v.equals(mPauseBtn)) {
				if (!mIsDialogShow) {
					showPauseDialog();
				}
			}
		};
	}
	
	private class PauseDialog extends Dialog {
		private ImageButton continueBtn = null, restartBtn = null, exitBtn = null;
		
		public PauseDialog(Context context) {
			// TODO Auto-generated constructor stub
			super(context);
		}
		
		@Override
		protected void onCreate(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onCreate(savedInstanceState);
			
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			setContentView(R.layout.dialog_layout_pause);
			getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			
			continueBtn = (ImageButton)findViewById(R.id.dialog_layout_continue_btn);
			restartBtn = (ImageButton)findViewById(R.id.dialog_layout_restart_btn);
			exitBtn = (ImageButton)findViewById(R.id.dialog_layout_exit_btn);
			
			ButtonEventHandler buttonEventHandler = new ButtonEventHandler();
			continueBtn.setOnClickListener(buttonEventHandler);
			restartBtn.setOnClickListener(buttonEventHandler);
			exitBtn.setOnClickListener(buttonEventHandler);
			
			setCanceledOnTouchOutside(false);
		}
		
		@Override
		public void onBackPressed() {
			// TODO Auto-generated method stub
		}

		private class ButtonEventHandler implements View.OnClickListener {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mIsDialogShow = false;
				
				int id = v.getId();
				if (id == R.id.dialog_layout_continue_btn) {
					mSurfaceView.resumeGameEngine();
					mGameAudioPlayer.resumeBGM();
					dismiss();
				} else if (id == R.id.dialog_layout_restart_btn) {
					mSurfaceView.stopGameEngine();
					mSurfaceView.startGameEngine((GameActivity)mContext);
					if (mBgmFlag) {
						mGameAudioPlayer.stopBGM();
						mGameAudioPlayer.startBGM(R.raw.game_bgm, true);
					}
					setScore(0);
					setTime(60);
					dismiss();
				} else if (id == R.id.dialog_layout_exit_btn) {
					dismiss();
					mActivityFinishFlag = true;
					startActivity(new Intent(GameActivity.this, MainActivity.class));
				}
			}
		}
	}
	
	private class TimeOverDialog extends Dialog {
		TimeOverAsyncTask timeOverAsyncTask = null;
		
		public TimeOverDialog(Context context, int gameScore, int gameRemoveObject) {
			// TODO Auto-generated constructor stub
			super(context);
			
			timeOverAsyncTask = new TimeOverAsyncTask(gameScore, gameRemoveObject);
		}
		
		@Override
		protected void onCreate(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onCreate(savedInstanceState);
			
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			setContentView(R.layout.dialog_layout_timeover);
			getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			
			setCanceledOnTouchOutside(false);
			
			timeOverAsyncTask.execute();
		}
		
		@Override
		public void onBackPressed() {
			// TODO Auto-generated method stub
		}
		
		private class TimeOverAsyncTask extends AsyncTask<Void, Void, Void> {
			private static final int SLEEP_TIME = 2000;
			
			private int gameScore = 0, gameRemoveObject = 0;
			
			public TimeOverAsyncTask(int gameScore, int gameRemoveObject) {
				// TODO Auto-generated constructor stub
				this.gameScore = gameScore;
				this.gameRemoveObject = gameRemoveObject;
			}
			
			@Override
			protected Void doInBackground(Void... params) {
				// TODO Auto-generated method stub
				SystemClock.sleep(SLEEP_TIME);
				publishProgress();
				return null;
			}
			
			@Override
			protected void onProgressUpdate(Void... values) {
				// TODO Auto-generated method stub
				dismiss();
				mActivityFinishFlag = true;
				
				Intent intent = new Intent(GameActivity.this, GameResultActivity.class);
				intent.putExtra(GameResultActivity.GAME_RESULT, gameScore);
				intent.putExtra(GameResultActivity.GAME_REMOVE_OBJECT, gameRemoveObject);
				startActivity(intent);
			}
		}
	}
}
