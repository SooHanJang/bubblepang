package team.madcat.game;

import java.util.Random;

import team.madcat.bubblepang.R;
import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Vibrator;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;

public class GameSurfaceView extends SurfaceView implements Callback {
	private Context mContext = null;
	private SurfaceHolder mSurfaceHolder = null;
	private GameEngine mGameEngine = null;
	
	private boolean mGameVibrateFlag = true;
	private Vibrator mVibrator = null;

	private boolean mGameSoundFlag = true;
	private SoundPool mGameEffectSoundPool = null;
	
	private int[] mTargetCollisionEffectSound = null;
	private int mSpeedUpCollisionEffectSound = 0;
	private int mSpeedDwCollisionEffectSound = 0;
	private int mClearCollisionEffectSound = 0;
	private int mBlindCollisionEffectSound = 0;
	
	public GameSurfaceView(Context context) {
		// TODO Auto-generated constructor stub
		super(context);
		
		mContext = context;
		mSurfaceHolder = getHolder();
		mSurfaceHolder.addCallback(this);
	}

	public void surfaceCreated(SurfaceHolder holder) { }
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) { }
	public void surfaceDestroyed(SurfaceHolder holder) { }
	
	public void startGameEngine(GameActivity gameActivity) {
		mGameEngine = new GameEngine(mContext, gameActivity, mSurfaceHolder);
		mGameEngine.execute();
		
		if (mGameVibrateFlag) {
			mVibrator = (Vibrator)mContext.getSystemService(Context.VIBRATOR_SERVICE);
		}
		
		if (mGameSoundFlag) {
			mGameEffectSoundPool = new SoundPool(6, AudioManager.STREAM_MUSIC, 0);

			mTargetCollisionEffectSound = new int[2];
			mTargetCollisionEffectSound[0] = mGameEffectSoundPool.load(mContext, R.raw.game_target_remove_1, 1);
			mTargetCollisionEffectSound[1] = mGameEffectSoundPool.load(mContext, R.raw.game_target_remove_2, 1);
			
			mSpeedDwCollisionEffectSound = mGameEffectSoundPool.load(mContext, R.raw.game_item_speeddw, 1);
			mSpeedUpCollisionEffectSound = mGameEffectSoundPool.load(mContext, R.raw.game_item_speedup, 1);
			
			mClearCollisionEffectSound = mGameEffectSoundPool.load(mContext, R.raw.game_item_clear, 1);
			mBlindCollisionEffectSound = mGameEffectSoundPool.load(mContext, R.raw.game_item_blind, 1);
		}
	}
	
	public void resumeGameEngine() {
		mGameEngine.engineResume();
	}
	
	public void pauseGameEngine() {
		mGameEngine.enginePause();
	}
	
	public void stopGameEngine() {
		mGameEngine.engineStop();
		mGameEngine = null;
		
		if (mGameEffectSoundPool != null) {
			mGameEffectSoundPool.release();
			mGameEffectSoundPool = null;
		}
	}
	
	public void setVibrateFlag(boolean vibrateFlag) {
		mGameVibrateFlag = vibrateFlag;
	}
	
	public void setSoundFlag(boolean soundFlag) {
		mGameSoundFlag = soundFlag;
	}
	
	private static final int THRESHOLD = 10;
	private float mPreX = 0, mPreY = 0;
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		float x = event.getX();
		float y = event.getY();
		
		boolean thresholdFlag = false;
		
		if (mPreX == 0 && mPreY == 0) {
			mPreX = x;
			mPreY = y;
			
			thresholdFlag = true;
		} else {
			if (Math.abs(mPreX - x) > THRESHOLD || Math.abs(mPreY- y) > THRESHOLD) {
				thresholdFlag = true;
			} else {
				thresholdFlag = false;
			}
		}
		
		if (thresholdFlag) {
			if (!mGameEngine.isPause()) {
				int eventType = event.getAction();
				switch (eventType) {
				case MotionEvent.ACTION_DOWN:
//				case MotionEvent.ACTION_MOVE:
					int collisionTargetType = mGameEngine.isCollisionOnTarget(event.getAction(), x, y);
					if (collisionTargetType != GameEngine.COLLISTION_NONE) {
						vibrate();
						playSound(collisionTargetType);
					}
					break;
				}
			}
			
			mPreX = x;
			mPreY = y;
		}
		
		return true;
	}
	
	private static final long VIBRATE_TIME = 200;
	private void vibrate() {
		if (mGameVibrateFlag) {
			mVibrator.vibrate(VIBRATE_TIME);
		}
	}
	
	private void playSound(int soundType) {
		if (mGameSoundFlag) {
			switch (soundType) {
			case GameEngine.COLLISTION_NORMAL:
				Random random = new Random();
				if (random.nextInt(2) == 0) {
					mGameEffectSoundPool.play(mTargetCollisionEffectSound[random.nextInt(2)], 2.0f, 2.0f, 0, 0, 1.0f);
				} else {
					mGameEffectSoundPool.play(mTargetCollisionEffectSound[random.nextInt(2)], 3.0f, 3.0f, 0, 0, 1.0f);
				}
				break;
			case GameEngine.COLLISTION_POS_SPEED:
				mGameEffectSoundPool.play(mSpeedDwCollisionEffectSound, 2.0f, 2.0f, 0, 0, 1.0f);
				break;
			case GameEngine.COLLISTION_POS_CLEAR:
				mGameEffectSoundPool.play(mClearCollisionEffectSound, 2.0f, 2.0f, 0, 0, 1.0f);
				break;
			case GameEngine.COLLISTION_NEG_SPEED:
				mGameEffectSoundPool.play(mSpeedUpCollisionEffectSound, 2.0f, 2.0f, 0, 0, 1.0f);
				break;
			case GameEngine.COLLISTION_NEG_BLIND:
				mGameEffectSoundPool.play(mBlindCollisionEffectSound, 2.0f, 2.0f, 0, 0, 1.0f);
				break;
			}
		}
	}
}