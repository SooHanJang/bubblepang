package team.madcat.game;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import team.madcat.bubblepang.R;
import team.madcat.game.object.GameObjectConstants;
import team.madcat.game.object.GameObjectTarget;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.SystemClock;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.WindowManager;

public class GameEngine extends AsyncTask<Void, Integer, Void> {
	public static final int GAME_TIME_LIMIT	= 60 * 1000;
	
	private static final int PAUSE_LOOP_INTERVAL = 100;
	private static final int GAME_LOOP_INTERVAL = 25;
	private static final int GAME_OBJECT_MAKE_INTERVAL = 1000;
	
	private static final int mGameTargetNumber = 3;

	private Context mContext = null;
	
	private static final String WAKELOCK_TAG = "bubblepang_wakelock";
	private WakeLock mWakeLock = null;
	
	private GameActivity mGameActivity = null;
	
	private boolean mRunFlag = false, mWaitFlag = true;
	
	private SurfaceHolder mSurfaceHolder = null;
	private float mScreenWidth = 0, mScreenHeight = 0;  
	private float mScreenWidthWeight = 0, mScreenHeightWeight = 0;
	
	private ArrayList<GameObjectTarget> mTargetObjectList = null;
	private ArrayList<GameObjectTarget> mBlindObjectList = null;
	
	private int mScore = 0;
	private int mRunningTime = 0;
	
	private int mCounterRemoveNormalObject = 0;
	
	//ITEM PROPERTIES.
	private static final int PERIOD_SPEED_CHANGE = 3000;
	private int mRemainSpeedChangeTime = 0;
	
	//COMBO PROPERTIES.
	private static final int PERIOD_COMBO = 800;
	private static final int COMBO_SCORE = 10;
	private static final int COMBO_IMAGE_DRAW_TIME = 500;
	
	private int mRemainComboTime = 0;
	private int mComboCounter = 0;
	private int mTempComboCounter = 0;
	
	private int mRemainComboImageDrawTime = 0;
	private boolean mIsDrawComboImage = false;
	
	private Bitmap mBackgroundBitmap = null;
	
	private Bitmap mComboImage = null;
	private Bitmap[] mComboNumber = null;
	
	private Bitmap[] mTargetNormalHugeBitmaps = null;
	private Bitmap[] mTargetNormalMidiBitmaps = null;
	private Bitmap[] mTargetNormalTinyBitmaps = null;
	
	private Bitmap[] mTargetItemSpeedUpBitmaps = null;
	private Bitmap[] mTargetItemSpeedDwBitmaps = null;
	
	private Bitmap[] mTargetItemBlindBitmaps = null;
	private Bitmap[] mTargetItemClearBitmaps = null;
	
	//Game Engine Constructor
	public GameEngine(Context context, GameActivity activity, SurfaceHolder surfaceHolder) {
		// TODO Auto-generated constructor stub
		mContext = context;
		mGameActivity = activity;
		
		PowerManager powerManager = (PowerManager)context.getSystemService(Context.POWER_SERVICE);
		mWakeLock = powerManager.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK, WAKELOCK_TAG);
		
		mSurfaceHolder = surfaceHolder;
		
		Display display = ((WindowManager)context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		mScreenWidth = display.getWidth();
		mScreenHeight = display.getHeight();
		mScreenWidthWeight = mScreenWidth / GameObjectConstants.DEFAULT_SCREEN_WIDTH;
		mScreenHeightWeight = mScreenHeight / GameObjectConstants.DEFAULT_SCREEN_HEIGHT;
		
		mGameActivity.setEnableMenuBtn(false);
	}
	
	//Game Engine Body
	@SuppressLint("Wakelock")
	@Override
	protected Void doInBackground(Void... params) {
		// TODO Auto-generated method stub
		//Acquire WakeLock.
		mWakeLock.acquire();
		
		//Initialize Game Engine Start.
		mRunFlag = true;
		mWaitFlag = false;
		
		mTargetObjectList = new ArrayList<GameObjectTarget>();
		mBlindObjectList = new ArrayList<GameObjectTarget>();
		GameObjectTarget targetObject = null;
		
		mComboImage = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.game_combo);
		mComboImage = Bitmap.createScaledBitmap(mComboImage, 215, 100, true);
		
		mComboNumber = new Bitmap[10];
		for (int i = 0; i < mComboNumber.length; i++) {
			mComboNumber[i] = BitmapFactory.decodeResource(mContext.getResources(), mContext.getResources().getIdentifier("game_combo_" + i, "drawable", mContext.getPackageName()));
			mComboNumber[i] = Bitmap.createScaledBitmap(mComboNumber[i], 52, 100, true);
		}
		
		double x = 0, y = 0;
		float w = 0, h = 0;
		
		for (int i = 0; i < mGameTargetNumber; i++) {
			mTargetObjectList.add(new GameObjectTarget(0, mScreenWidth, mScreenHeight));
		}
		
		mBackgroundBitmap = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.game_background);
		mBackgroundBitmap = Bitmap.createScaledBitmap(mBackgroundBitmap, (int)mScreenWidth, (int)mScreenHeight, true);
		
		mTargetNormalHugeBitmaps = new Bitmap[3];
		mTargetNormalMidiBitmaps = new Bitmap[3];
		mTargetNormalTinyBitmaps = new Bitmap[3];
		
		mTargetItemSpeedUpBitmaps = new Bitmap[2];
		mTargetItemSpeedDwBitmaps = new Bitmap[2];
		
		mTargetItemBlindBitmaps = new Bitmap[3];
		mTargetItemClearBitmaps = new Bitmap[2];
		
		Resources resources = mContext.getResources();
		String packageName = mContext.getPackageName();
		
		for (int i = 0; i < 3; i++) {
			if (i == 0) {
				mTargetNormalHugeBitmaps[i] = BitmapFactory.decodeResource(resources, resources.getIdentifier("game_target_huge_" + i, "drawable", packageName));
				mTargetNormalHugeBitmaps[i] = Bitmap.createScaledBitmap(mTargetNormalHugeBitmaps[i], (int)(GameObjectConstants.SIZE_OBJECT_HUGE * mScreenWidthWeight), (int)(GameObjectConstants.SIZE_OBJECT_HUGE * mScreenHeightWeight), true);
				mTargetNormalMidiBitmaps[i] = BitmapFactory.decodeResource(resources, resources.getIdentifier("game_target_midi_" + i, "drawable", packageName));
				mTargetNormalMidiBitmaps[i] = Bitmap.createScaledBitmap(mTargetNormalMidiBitmaps[i], (int)(GameObjectConstants.SIZE_OBJECT_MIDI * mScreenWidthWeight), (int)(GameObjectConstants.SIZE_OBJECT_MIDI * mScreenHeightWeight), true);
				mTargetNormalTinyBitmaps[i] = BitmapFactory.decodeResource(resources, resources.getIdentifier("game_target_tiny_" + i, "drawable", packageName));
				mTargetNormalTinyBitmaps[i] = Bitmap.createScaledBitmap(mTargetNormalTinyBitmaps[i], (int)(GameObjectConstants.SIZE_OBJECT_TINY * mScreenWidthWeight), (int)(GameObjectConstants.SIZE_OBJECT_TINY * mScreenHeightWeight), true);
				
				mTargetItemSpeedUpBitmaps[i] = BitmapFactory.decodeResource(resources, resources.getIdentifier("game_target_speedup_" + i, "drawable", packageName));
				mTargetItemSpeedUpBitmaps[i] = Bitmap.createScaledBitmap(mTargetItemSpeedUpBitmaps[i], (int)(GameObjectConstants.SIZE_OBJECT_MIDI * mScreenWidthWeight), (int)(GameObjectConstants.SIZE_OBJECT_MIDI * mScreenHeightWeight), true);
				mTargetItemSpeedDwBitmaps[i] = BitmapFactory.decodeResource(resources, resources.getIdentifier("game_target_speeddw_" + i, "drawable", packageName));
				mTargetItemSpeedDwBitmaps[i] = Bitmap.createScaledBitmap(mTargetItemSpeedDwBitmaps[i], (int)(GameObjectConstants.SIZE_OBJECT_MIDI * mScreenWidthWeight), (int)(GameObjectConstants.SIZE_OBJECT_MIDI * mScreenHeightWeight), true);
				mTargetItemBlindBitmaps[i] = BitmapFactory.decodeResource(resources, resources.getIdentifier("game_target_blind_" + i, "drawable", packageName));
				mTargetItemBlindBitmaps[i] = Bitmap.createScaledBitmap(mTargetItemBlindBitmaps[i], (int)(GameObjectConstants.SIZE_OBJECT_MIDI * mScreenWidthWeight), (int)(GameObjectConstants.SIZE_OBJECT_MIDI * mScreenHeightWeight), true);
				mTargetItemClearBitmaps[i] = BitmapFactory.decodeResource(resources, resources.getIdentifier("game_target_clear_" + i, "drawable", packageName));
				mTargetItemClearBitmaps[i] = Bitmap.createScaledBitmap(mTargetItemClearBitmaps[i], (int)(GameObjectConstants.SIZE_OBJECT_MIDI * mScreenWidthWeight), (int)(GameObjectConstants.SIZE_OBJECT_MIDI * mScreenHeightWeight), true);
			} else {
				mTargetNormalHugeBitmaps[i] = BitmapFactory.decodeResource(resources, resources.getIdentifier("game_target_huge_" + i, "drawable", packageName));
				mTargetNormalHugeBitmaps[i] = Bitmap.createScaledBitmap(mTargetNormalHugeBitmaps[i], (int)(GameObjectConstants.SIZE_OBJECT_HUGE * mScreenWidthWeight * 1.5f), (int)(GameObjectConstants.SIZE_OBJECT_HUGE * mScreenHeightWeight * 1.5f), true);
				mTargetNormalMidiBitmaps[i] = BitmapFactory.decodeResource(resources, resources.getIdentifier("game_target_midi_" + i, "drawable", packageName));
				mTargetNormalMidiBitmaps[i] = Bitmap.createScaledBitmap(mTargetNormalMidiBitmaps[i], (int)(GameObjectConstants.SIZE_OBJECT_MIDI * mScreenWidthWeight * 1.5f), (int)(GameObjectConstants.SIZE_OBJECT_MIDI * mScreenHeightWeight * 1.5f), true);
				mTargetNormalTinyBitmaps[i] = BitmapFactory.decodeResource(resources, resources.getIdentifier("game_target_tiny_" + i, "drawable", packageName));
				mTargetNormalTinyBitmaps[i] = Bitmap.createScaledBitmap(mTargetNormalTinyBitmaps[i], (int)(GameObjectConstants.SIZE_OBJECT_TINY * mScreenWidthWeight * 1.5f), (int)(GameObjectConstants.SIZE_OBJECT_TINY * mScreenHeightWeight * 1.5f), true);
				
				if (i < 2) {
					mTargetItemSpeedUpBitmaps[i] = BitmapFactory.decodeResource(resources, resources.getIdentifier("game_target_speedup_" + i, "drawable", packageName));
					mTargetItemSpeedUpBitmaps[i] = Bitmap.createScaledBitmap(mTargetItemSpeedUpBitmaps[i], (int)(GameObjectConstants.SIZE_OBJECT_MIDI * mScreenWidthWeight * 1.5f), (int)(GameObjectConstants.SIZE_OBJECT_MIDI * mScreenHeightWeight * 1.5f), true);
					mTargetItemSpeedDwBitmaps[i] = BitmapFactory.decodeResource(resources, resources.getIdentifier("game_target_speeddw_" + i, "drawable", packageName));
					mTargetItemSpeedDwBitmaps[i] = Bitmap.createScaledBitmap(mTargetItemSpeedDwBitmaps[i], (int)(GameObjectConstants.SIZE_OBJECT_MIDI * mScreenWidthWeight * 1.5f), (int)(GameObjectConstants.SIZE_OBJECT_MIDI * mScreenHeightWeight * 1.5f), true);
					mTargetItemBlindBitmaps[i] = BitmapFactory.decodeResource(resources, resources.getIdentifier("game_target_blind_" + i, "drawable", packageName));
					mTargetItemBlindBitmaps[i] = Bitmap.createScaledBitmap(mTargetItemBlindBitmaps[i], (int)(GameObjectConstants.SIZE_OBJECT_MIDI * mScreenWidthWeight * 1.5f), (int)(GameObjectConstants.SIZE_OBJECT_MIDI * mScreenHeightWeight * 1.5f), true);
					mTargetItemClearBitmaps[i] = BitmapFactory.decodeResource(resources, resources.getIdentifier("game_target_clear_" + i, "drawable", packageName));
					mTargetItemClearBitmaps[i] = Bitmap.createScaledBitmap(mTargetItemClearBitmaps[i], (int)(GameObjectConstants.SIZE_OBJECT_MIDI * mScreenWidthWeight * 1.5f), (int)(GameObjectConstants.SIZE_OBJECT_MIDI * mScreenHeightWeight * 1.5f), true);
				}
			}
		}
		
		mTargetItemBlindBitmaps[2] = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.game_target_blind_2);
		mTargetItemBlindBitmaps[2] = Bitmap.createScaledBitmap(mTargetItemBlindBitmaps[2], (int)(GameObjectConstants.SIZE_OBJECT_BLIND * mScreenWidthWeight), (int)(GameObjectConstants.SIZE_OBJECT_BLIND * mScreenHeightWeight), true);
		//Initialize Game Engine Done.
		
		//Draw Ready & Go Bitmap in Screen Start.
		Canvas canvas = null;
		Paint paint = new Paint();
		
		Bitmap readyBitmap = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.game_ready);
		readyBitmap = Bitmap.createScaledBitmap(readyBitmap, 300, 300, true);
		
		Bitmap goBitmap = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.game_go);
		goBitmap = Bitmap.createScaledBitmap(goBitmap, 300, 300, true);
		
		canvas = mSurfaceHolder.lockCanvas();
		
		try {
			synchronized (mSurfaceHolder) {
				if (canvas != null) {
					paint.setAntiAlias(true);
					canvas.drawBitmap(mBackgroundBitmap, 0, 0, paint);
					canvas.drawBitmap(readyBitmap, mScreenWidth / 2 - 150, mScreenHeight / 2 - 150, paint);
				}
			}
		} finally {
			if (canvas != null) {
				mSurfaceHolder.unlockCanvasAndPost(canvas);
			}
		}
		
		SystemClock.sleep(2000);
		
		canvas = mSurfaceHolder.lockCanvas();
		
		try {
			synchronized (mSurfaceHolder) {
				if (canvas != null) {
					paint.setAntiAlias(true);
					canvas.drawBitmap(mBackgroundBitmap, 0, 0, paint);
					canvas.drawBitmap(goBitmap, mScreenWidth / 2 - 150, mScreenHeight / 2 - 150, paint);
				}
			}
		} finally {
			if (canvas != null) {
				mSurfaceHolder.unlockCanvasAndPost(canvas);
			}
		}
		
		mGameActivity.playGameStartSound();
		SystemClock.sleep(1000);
		
		publishProgress(PROGRESS_READY_COMPLETE);
		
		//Bitmap allocate memory release.
		readyBitmap.recycle();
		goBitmap.recycle();
		//Draw Ready & Go Bitmap in Screen End.
		
		//Start Game Timer for Check Time over.
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				if (!mWaitFlag) {
					mRunningTime = mRunningTime + GAME_LOOP_INTERVAL;
					
					if (mRunningTime % GAME_OBJECT_MAKE_INTERVAL == 0) {
						for (int i = 0; i < mGameTargetNumber; i++) {
							mTargetObjectList.add(new GameObjectTarget(mRunningTime, mScreenWidth, mScreenHeight));
						}
					}
					
					for (int i = 0; i < mTargetObjectList.size(); i++) {
						if (mTargetObjectList.get(i).getAnimationFlag()) {
							mTargetObjectList.get(i).decreaseAnimationTime(GAME_LOOP_INTERVAL);
						}
					}
					
					for (int i = 0; i < mBlindObjectList.size(); i++) {
						mBlindObjectList.get(i).decreaseAnimationTime(GAME_LOOP_INTERVAL);
					}
					
					if (mRemainSpeedChangeTime > 0) {
						mRemainSpeedChangeTime = mRemainSpeedChangeTime - GAME_LOOP_INTERVAL;
					} else if (mRemainSpeedChangeTime < 0) {
						mRemainSpeedChangeTime = mRemainSpeedChangeTime + GAME_LOOP_INTERVAL;
					}
					
					if (mRemainComboTime > 0) {
						mRemainComboTime = mRemainComboTime - GAME_LOOP_INTERVAL;
					} else {
						mComboCounter = 0;
					}
					
					if (mRemainComboImageDrawTime > 0) {
						mRemainComboImageDrawTime = mRemainComboImageDrawTime - GAME_LOOP_INTERVAL;
					} else {
						mIsDrawComboImage = false;
					}
					
					if (mRunningTime >= GAME_TIME_LIMIT) {
						//Game Over!
						publishProgress(PROGRESS_TIMEOVER);
						enginePause();
						engineStop();
					} else {
						publishProgress(PROGRESS_INFORMATION);
					}
				}
			}
		}, 0, GAME_LOOP_INTERVAL);
		
		//Run Game Draw Process.
		while (mRunFlag) {
			while (!mWaitFlag) {	//if game state is pause, not draw game screen.
				canvas = mSurfaceHolder.lockCanvas();
				try {
					synchronized (mSurfaceHolder) {
						if (canvas != null) {
							paint.setAntiAlias(true);
							canvas.drawBitmap(mBackgroundBitmap, 0, 0, paint);
							
							//Draw Game Objects.
							for (int i = 0; i < mTargetObjectList.size(); i++) {
								targetObject = mTargetObjectList.get(i);
								
								x = targetObject.getX();
								y = targetObject.getY();
								w = targetObject.getWidth();
								h = targetObject.getHeight();
								
								switch (targetObject.getObjectType()) {
								case GameObjectConstants.TYPE_NORMAL_HUGE:
									if (!targetObject.getAnimationFlag()) {
										canvas.drawBitmap(mTargetNormalHugeBitmaps[0], (float)x, (float)y, paint);
									} else {
										if (targetObject.getRemainAnimationTime() > GameObjectConstants.ANIMATION_TIME_NORMAL / 3 * 2) {
											canvas.drawBitmap(mTargetNormalHugeBitmaps[1], (float)x - w / 4, (float)y - h / 4, paint);
										} else {
											canvas.drawBitmap(mTargetNormalHugeBitmaps[2], (float)x - w / 4, (float)y - h / 4, paint);
										}
									}
									break;
								case GameObjectConstants.TYPE_NORMAL_TINY:
									if (!targetObject.getAnimationFlag()) {
										canvas.drawBitmap(mTargetNormalMidiBitmaps[0], (float)x, (float)y, paint);
									} else {
										if (targetObject.getRemainAnimationTime() > GameObjectConstants.ANIMATION_TIME_NORMAL / 3 * 2) {
											canvas.drawBitmap(mTargetNormalMidiBitmaps[1], (float)x - w / 4, (float)y - h / 4, paint);
										} else {
											canvas.drawBitmap(mTargetNormalMidiBitmaps[2], (float)x - w / 4, (float)y - h / 4, paint);
										}
									}
									break;
								case GameObjectConstants.TYPE_NORMAL_MIDI:
									if (!targetObject.getAnimationFlag()) {
										canvas.drawBitmap(mTargetNormalTinyBitmaps[0], (float)x, (float)y, paint);
									} else {
										if (targetObject.getRemainAnimationTime() > GameObjectConstants.ANIMATION_TIME_NORMAL / 3 * 2) {
											canvas.drawBitmap(mTargetNormalTinyBitmaps[1], (float)x - w / 4, (float)y - h / 4, paint);
										} else {
											canvas.drawBitmap(mTargetNormalTinyBitmaps[2], (float)x - w / 4, (float)y - h / 4, paint);
										}
									}
									break;
								case GameObjectConstants.TYPE_POSITIVE_SPEED:
									if (!targetObject.getAnimationFlag()) {
										canvas.drawBitmap(mTargetItemSpeedDwBitmaps[0], (float)x, (float)y, paint);
									} else {
										canvas.drawBitmap(mTargetItemSpeedDwBitmaps[1], (float)x - w / 4, (float)y - h / 4, paint);
									}
									break;
								case GameObjectConstants.TYPE_POSITIVE_CLEAR:
									if (!targetObject.getAnimationFlag()) {
										canvas.drawBitmap(mTargetItemClearBitmaps[0], (float)x, (float)y, paint);
									} else {
										canvas.drawBitmap(mTargetItemClearBitmaps[1], (float)x - w / 4, (float)y - h / 4, paint);
									}
									break;
								case GameObjectConstants.TYPE_NEGATIVE_SPEED:
									if (!targetObject.getAnimationFlag()) {
										canvas.drawBitmap(mTargetItemSpeedUpBitmaps[0], (float)x, (float)y, paint);
									} else {
										canvas.drawBitmap(mTargetItemSpeedUpBitmaps[1], (float)x - w / 4, (float)y - h / 4, paint);
									}
									break;
								case GameObjectConstants.TYPE_NEGATIVE_BLIND:
									if (!targetObject.getAnimationFlag()) {
										canvas.drawBitmap(mTargetItemBlindBitmaps[0], (float)x, (float)y, paint);
									}
									break;
								}
								
								//Move Game Object.
								if (mRemainSpeedChangeTime > 0) {
									targetObject.moveObject(GameObjectTarget.SPEED_UP);
								} else if (mRemainSpeedChangeTime < 0) {
									targetObject.moveObject(GameObjectTarget.SPEED_DW);
								} else {
									targetObject.moveObject();
								}
							}
							
							for (int i = 0; i < mBlindObjectList.size(); i++) {
								targetObject = mBlindObjectList.get(i);
								
								x = targetObject.getX();
								y = targetObject.getY();
								w = targetObject.getWidth();
								h = targetObject.getHeight();
								
								if (targetObject.getRemainAnimationTime() > GameObjectConstants.ANIMATION_TIME_BLIND / 35 * 30) {
									canvas.drawBitmap(mTargetItemBlindBitmaps[1], (float)x - w / 4, (float)y - h / 4, paint);
								} else {
									canvas.drawBitmap(mTargetItemBlindBitmaps[2], (float)x - (w / 2), (float)y - (h / 2), paint);
								}
							}
							
							//Draw Combo Image.
							if (mIsDrawComboImage) {
								String comboNumberStr = null;
								
								if (mComboCounter == 0) {
									comboNumberStr = Integer.toString(mTempComboCounter);
								} else {
									comboNumberStr = Integer.toString(mComboCounter);
								}
								
								switch (comboNumberStr.length()) {
								case 1:	//Combo 1~9
									if (comboNumberStr.charAt(0) != '0') {
										canvas.drawBitmap(mComboImage, mScreenWidth / 2 - 133.5f, mScreenHeight / 2 - 50, paint);
										canvas.drawBitmap(mComboNumber[Character.digit(comboNumberStr.charAt(0), 10)], mScreenWidth / 2 + 81.5f, mScreenHeight / 2 - 50, paint);
									}
									break;
								case 2: //Combo 10~99
									canvas.drawBitmap(mComboImage, mScreenWidth / 2 - 159.5f, mScreenHeight / 2 - 50, paint);
									canvas.drawBitmap(mComboNumber[Character.digit(comboNumberStr.charAt(0), 10)], mScreenWidth / 2 + 55.5f, mScreenHeight / 2 - 50, paint);
									canvas.drawBitmap(mComboNumber[Character.digit(comboNumberStr.charAt(1), 10)], mScreenWidth / 2 + 55.5f + 52.0f, mScreenHeight / 2 - 50, paint);
									break;
								case 3: //Combo 100~999
									canvas.drawBitmap(mComboImage, mScreenWidth / 2 - 185.5f, mScreenHeight / 2 - 50, paint);
									canvas.drawBitmap(mComboNumber[Character.digit(comboNumberStr.charAt(0), 10)], mScreenWidth / 2 + 29.5f, mScreenHeight / 2 - 50, paint);
									canvas.drawBitmap(mComboNumber[Character.digit(comboNumberStr.charAt(1), 10)], mScreenWidth / 2 + 29.5f + 52.0f, mScreenHeight / 2 - 50, paint);
									canvas.drawBitmap(mComboNumber[Character.digit(comboNumberStr.charAt(2), 10)], mScreenWidth / 2 + 29.5f + 52.0f + 52.0f, mScreenHeight / 2 - 50, paint);
									break;
								}
							}
						}
					}
				} finally {
					if (canvas != null) {
						mSurfaceHolder.unlockCanvasAndPost(canvas);
					}
				}
				
				//Check Out of Screen Objects.
				for (int i = 0; i < mTargetObjectList.size(); i++) {
					targetObject = mTargetObjectList.get(i);
					x = targetObject.getX();
					y = targetObject.getY();
					w = targetObject.getWidth();
					h = targetObject.getHeight();
					
					if ((x + w < 0 || x > mScreenWidth) || ((y + h < 0) || y > mScreenHeight)) {
						//Out of Screen
						if (targetObject.getObjectType() == GameObjectConstants.TYPE_NORMAL_TINY ||
							targetObject.getObjectType() == GameObjectConstants.TYPE_NORMAL_MIDI ||
							targetObject.getObjectType() == GameObjectConstants.TYPE_NORMAL_HUGE) {
							
							if (mRemainComboImageDrawTime > 0) {
								mTempComboCounter = mComboCounter;
							}
							
							//if occurred out of screen object, combo record initialize.
							mRemainComboTime = 0;
							mComboCounter = 0;
						}
						
						//remove object.
						mTargetObjectList.remove(i--);
					}
				}
				
				//if animation time is zero, remove.
				for (int i = 0; i < mTargetObjectList.size(); i++) {
					targetObject = mTargetObjectList.get(i);
					if (targetObject.getAnimationFlag() && targetObject.getRemainAnimationTime() == 0) {
						mTargetObjectList.remove(i--);
					}
				}
				
				for (int i = 0; i < mBlindObjectList.size(); i++) {
					targetObject = mBlindObjectList.get(i);
					if (targetObject.getRemainAnimationTime() == 0) {
						mBlindObjectList.remove(i--);
					}
				}
				
				SystemClock.sleep(GAME_LOOP_INTERVAL);
			}
			
			SystemClock.sleep(PAUSE_LOOP_INTERVAL);
		}
		
		//Stop Game Timer.
		timer.cancel();
		timer.purge();
		
		//Allocated Memory Released.
		mBackgroundBitmap.recycle();
		mComboImage.recycle();
		for (int i = 0; i < 10; i++) {
			mComboNumber[i].recycle();
		}
		
		for (int i = 0; i < 3; i++) {
			mTargetNormalHugeBitmaps[i].recycle();
			mTargetNormalMidiBitmaps[i].recycle();
			mTargetNormalTinyBitmaps[i].recycle();
			
			if (i < 2) {
				mTargetItemSpeedDwBitmaps[i].recycle();
				mTargetItemSpeedUpBitmaps[i].recycle();
				mTargetItemClearBitmaps[i].recycle();
			}
			
			mTargetItemBlindBitmaps[i].recycle();
		}
		
		//Release wakelock.
		mWakeLock.release();
		
		//clear arraylists.
		mTargetObjectList.clear();
		mTargetObjectList = null;
		
		mBlindObjectList.clear();
		mBlindObjectList = null;
		
		return null;
	}
	
	private static final int PROGRESS_READY_COMPLETE	= 0;
	private static final int PROGRESS_INFORMATION		= 1;
	private static final int PROGRESS_TIMEOVER			= 2;
	
	//Update Information.
	@Override
	protected void onProgressUpdate(Integer... values) {
		// TODO Auto-generated method stub
		switch (values[0]) {
		case PROGRESS_READY_COMPLETE:
			mGameActivity.setEnableMenuBtn(true);
			break;
		case PROGRESS_INFORMATION:
			mGameActivity.setScore(mScore);
			mGameActivity.setTime((GAME_TIME_LIMIT - mRunningTime) / 1000);
			break;
		case PROGRESS_TIMEOVER:
			mGameActivity.showTimeOverDialog(mScore, mCounterRemoveNormalObject);
			break;
		}
	}
	
	public void engineResume() {
		mWaitFlag = false;
	}
	
	public void enginePause() {
		mWaitFlag = true;
	}
	
	public void engineStop() {
		mRunFlag = false;
	}
	
	public boolean isStart() {
		return mRunFlag;
	}
	
	public boolean isPause() {
		return mWaitFlag;
	}
	
	public static final int COLLISTION_NONE			= 0;
	public static final int COLLISTION_NORMAL		= 1;
	public static final int COLLISTION_POS_SPEED	= 2;
	public static final int COLLISTION_POS_CLEAR	= 3;
	public static final int COLLISTION_NEG_SPEED	= 4;
	public static final int COLLISTION_NEG_BLIND	= 5;

	/**
	 * check collision, between touch x, y point and all objects in arraylist.
	 * @param eventType
	 * @param x
	 * @param y
	 * @return collided object type.
	 */
	public int isCollisionOnTarget(int eventType, float x, float y) {
		RectF eventArea = new RectF((float)(x - 30), (float)(y - 30), (float)(x + 30), (float)(y + 30));
		RectF targetObjectArea = new RectF();
		double targetObjectX = 0, targetObjectY = 0;
		float targetObjectW = 0, targetObjectH = 0;
		
		GameObjectTarget targetObject = null;
		for (int i = mTargetObjectList.size(); i > 0; i--) {
			targetObject = mTargetObjectList.get(i-1);
			if (!targetObject.getAnimationFlag()) {
				targetObjectX = targetObject.getX();
				targetObjectY = targetObject.getY();
				targetObjectW = targetObject.getWidth();
				targetObjectH = targetObject.getHeight();
				
				targetObjectArea.left = (float)(targetObjectX);
				targetObjectArea.top = (float)(targetObjectY);
				targetObjectArea.right = (float)(targetObjectX + targetObjectW);
				targetObjectArea.bottom = (float)(targetObjectY + targetObjectH);
				
				if (targetObjectArea.intersect(eventArea)) {
					switch (targetObject.getObjectType()) {
					case GameObjectConstants.TYPE_NORMAL_TINY:
					case GameObjectConstants.TYPE_NORMAL_MIDI:
					case GameObjectConstants.TYPE_NORMAL_HUGE:
						targetObject.setAnimationFlag(true);
						mScore = mScore + targetObject.getObjectScore();
						
						if (mRemainComboTime > 0) {
							mComboCounter++;
							mScore = mScore + (mComboCounter * COMBO_SCORE);
							
							mIsDrawComboImage = true;
							mRemainComboImageDrawTime = COMBO_IMAGE_DRAW_TIME;
						}
						mRemainComboTime = PERIOD_COMBO;
						mCounterRemoveNormalObject++;
						return COLLISTION_NORMAL;
					case GameObjectConstants.TYPE_POSITIVE_SPEED:
						targetObject.setAnimationFlag(true);
						mRemainSpeedChangeTime = mRemainSpeedChangeTime - PERIOD_SPEED_CHANGE;
						return COLLISTION_POS_SPEED;
					case GameObjectConstants.TYPE_POSITIVE_CLEAR:
						targetObject.setAnimationFlag(true);
						
						for (int j = 0; j < mTargetObjectList.size(); j++) {
							targetObject = mTargetObjectList.get(j);
							if (!targetObject.getAnimationFlag()) {
								targetObject.setAnimationFlag(true);
								
								if (targetObject.isItemTargetObject()) {
									targetObject.setRemainAnimationTime(0);
								} else {
									mScore = mScore + targetObject.getObjectScore();
								}
							}
						}
						return COLLISTION_POS_CLEAR;
					case GameObjectConstants.TYPE_NEGATIVE_SPEED:
						targetObject.setAnimationFlag(true);
						mRemainSpeedChangeTime = mRemainSpeedChangeTime + PERIOD_SPEED_CHANGE;
						return COLLISTION_NEG_SPEED;
					case GameObjectConstants.TYPE_NEGATIVE_BLIND:
						targetObject.setAnimationFlag(true);
						mTargetObjectList.remove(i-1);
						mBlindObjectList.add(targetObject);
						return COLLISTION_NEG_BLIND;
					}
				}
			}
		}
		
		return COLLISTION_NONE;
	}
	
	public int getScore() {
		return mScore;
	}
}
