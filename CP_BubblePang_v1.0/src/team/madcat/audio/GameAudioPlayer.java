package team.madcat.audio;

import android.content.Context;
import android.media.MediaPlayer;

public class GameAudioPlayer {
	private Context mContext = null;
	private MediaPlayer mMediaPlayer = null;
	
	public GameAudioPlayer(Context context) {
		// TODO Auto-generated constructor stub
		mContext = context;
	}

	public void startBGM(int audioId, boolean repeat) {
		mMediaPlayer = MediaPlayer.create(mContext, audioId);
		mMediaPlayer.setLooping(repeat);
		mMediaPlayer.start();
	}
	
	public void resumeBGM() {
		if (mMediaPlayer != null) {
			mMediaPlayer.start();
		}
	}
	
	public void pauseBGM() {
		if (mMediaPlayer != null) {
			mMediaPlayer.pause();
		}
	}
	
	public void stopBGM() {
		if (mMediaPlayer != null) {
			mMediaPlayer.stop();
			mMediaPlayer.release();
			mMediaPlayer = null;
		}
	}
	
	public boolean isPlaying() {
		return mMediaPlayer.isPlaying();
	}
}
