package team.madcat.facebook;

import team.madcat.constants.Constants;
import team.madcat.facebook.AsyncFacebookRunner;
import team.madcat.facebook.BaseRequestListener;
import team.madcat.facebook.DialogError;
import team.madcat.facebook.Facebook;
import team.madcat.facebook.Facebook.DialogListener;
import team.madcat.facebook.FacebookError;
import team.madcat.facebook.SessionEvents;
import team.madcat.facebook.SessionEvents.AuthListener;
import team.madcat.facebook.SessionEvents.LogoutListener;
import team.madcat.facebook.SessionStore;
import team.madcat.bubblepang.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

public class FaceBookLogin {
	
//	private static final String TAG = "FaceBookLogin";
	
	private Facebook mFacebook;
	private AsyncFacebookRunner mAsyncRunner;
	private static final String[] PERMISSIONS = new String[] {"publish_stream", "read_stream", "offline_access"};
	
	private Context mContext;
	private Activity mActivity;
	private String mText;
	
	private ProgressDialog mProgress;

	public FaceBookLogin(Context context, Activity activity) {
		this.mContext = context;
		this.mActivity = activity;
	}
	
	public FaceBookLogin(Context context, Activity activity, String text) {
		this.mContext = context;
		this.mActivity = activity;
		this.mText = text;
	}
	
	public void login() {
		mFacebook = new Facebook(Constants.FACEBOOK_APP_KEY);		// FaceBook 에 등록된 앱의 아이디 설정
		
		mAsyncRunner = new AsyncFacebookRunner(mFacebook);	// 설정된 앱 아이디를 바탕으로 세션 초기화
		
		SessionStore.restore(mFacebook, mContext.getApplicationContext());
	    SessionEvents.addAuthListener(new FaceBookAuthListener());
	    SessionEvents.addLogoutListener(new FaceBookLogoutListener());
	    
		mFacebook.authorize(mActivity, PERMISSIONS, new LoginDialogListener());
	}
	
	private class LoginDialogListener implements DialogListener {

		public void onComplete(Bundle values) {		// 로그인 다이얼로그가 성공 했을 때 호출
//			Log.d(TAG, "로그인 다이얼로그 성공");
			
			 final EditText fortune_text = new EditText(mContext);
			 fortune_text.setText(mText.toString());
			 
			 AlertDialog.Builder builder =  new AlertDialog.Builder(mContext);
			 builder.setTitle(mContext.getResources().getString(R.string.dialog_title_sns_registe_facebook)).setView(fortune_text)
			    .setPositiveButton(mContext.getResources().getString(R.string.dialog_ok), new DialogInterface.OnClickListener() {
			     public void onClick(DialogInterface dialog, int whichButton) {
			    	 
			    	 mProgress = ProgressDialog.show(mContext, "", mContext.getResources().getString(R.string.progress_create_data));
			    	 
				     Bundle params = new Bundle();   
				     params.putString("message", fortune_text.getText().toString());   
				     mAsyncRunner.request("me/feed", params, "POST", new MessageRequestListener(), null);
			     }
			    }).setNegativeButton(mContext.getResources().getString(R.string.dialog_cancel), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						dialog.dismiss();
					}
				})
			    .create().show();  
		}

		public void onFacebookError(FacebookError e) {
			final String errorMessage = e.getMessage();
//			Log.d(TAG, "onFacebookError : " + errorMessage);
			
			mActivity.runOnUiThread(new Runnable() {
                public void run() {
                	Toast.makeText(mContext, "FaceBook Error" + errorMessage, Toast.LENGTH_SHORT).show();
                }
            });
		}
		public void onError(DialogError e) {
			final String errorMessage = e.getMessage();
//			Log.d(TAG, "onError : " + errorMessage);
			
			mActivity.runOnUiThread(new Runnable() {
                public void run() {
                	Toast.makeText(mContext, "FaceBook onError" + errorMessage, Toast.LENGTH_SHORT).show();
                }
            });
		}
		public void onCancel() {}
	}
	
	public class FaceBookAuthListener implements AuthListener {
        public void onAuthSucceed() {
//        	Log.d(TAG, "로그인 되었습니다.");
//        	Toast.makeText(mContext, "로그인 되었습니다.", Toast.LENGTH_SHORT).show();
        }

        public void onAuthFail(String error) {
//        	Log.d(TAG, "로그인 실패");
//            Toast.makeText(mContext, "로그인에 실패하였습니다.", Toast.LENGTH_SHORT).show();
        }
    }

    public class FaceBookLogoutListener implements LogoutListener {
        public void onLogoutBegin() {
//        	Log.d(TAG, "로그아웃 시작");
//            Toast.makeText(mContext, "로그아웃 중입니다.", Toast.LENGTH_SHORT).show();
        }

        public void onLogoutFinish() {
//        	Log.d(TAG, "로그아웃 완료");
//        	Toast.makeText(mContext, "로그아웃 되었습니다.", Toast.LENGTH_SHORT).show();
        }
    }
    
    public class MessageRequestListener extends BaseRequestListener {

		public void onComplete(String response, Object state) {
//			Log.d(TAG, "메시지 등록 후 액션을 이곳에 구현합니다.");
			mActivity.runOnUiThread(new Runnable() {
                public void run() {
                	mProgress.dismiss();
                	Toast.makeText(mContext, mContext.getResources().getString(R.string.toast_succeed_registe_post), Toast.LENGTH_SHORT).show();
                }
            });
		}
	}
}
