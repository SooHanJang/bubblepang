package team.madcat.trophy;

import team.madcat.bubblepang.MainActivity;
import team.madcat.bubblepang.R;
import team.madcat.constants.Constants;
import team.madcat.utils.Utils;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

public class TrophyListActivity extends Activity {
	private static final String PREFIX = "trophy_layout_";
	private static final String TROPHY = "trophy_";
	private static final String SELECT = "slt_";
	
	private boolean[] mTrophyLockFlag = null;
	private ImageButton[] mTrophyImageButtons = null;
	private ImageView[] mTrophySltImageViews = null;
	private int[] mTrophyImages = null;
	private int[] mTrophyExplainHintImages = null;
	private int[] mTrophyExplainAchieveImages = null;
	
	private int mCurrentSelectedIndex = 0;
	
	private ImageView mTrophyExplainView = null;
	private ImageButton mBackBtn = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_layout_trophy);
		
		mTrophyLockFlag = new boolean[9];
		mTrophyImageButtons = new ImageButton[9];
		mTrophySltImageViews = new ImageView[9];
		
		ImageButtonEventHandler imageButtonEventHandler = new ImageButtonEventHandler();
		
		SharedPreferences pref = getSharedPreferences(Constants.PREFERENCE_NAME, Context.MODE_PRIVATE);
		mTrophyLockFlag[0] = pref.getBoolean(Constants.TROPHY_GAME_SCORE_5000, false);
		mTrophyLockFlag[1] = pref.getBoolean(Constants.TROPHY_GAME_SCORE_10000, false);
		mTrophyLockFlag[2] = pref.getBoolean(Constants.TROPHY_GAME_SCORE_30000, false);
		mTrophyLockFlag[3] = pref.getBoolean(Constants.TROPHY_REMOVE_NORMAL_500, false);
		mTrophyLockFlag[4] = pref.getBoolean(Constants.TROPHY_REMOVE_NORMAL_1000, false);
		mTrophyLockFlag[5] = pref.getBoolean(Constants.TROPHY_REMOVE_NORMAL_2000, false);
		mTrophyLockFlag[6] = pref.getBoolean(Constants.TROPHY_GAME_PLAY_1HOUR, false);
		mTrophyLockFlag[7] = pref.getBoolean(Constants.TROPHY_GAME_PLAY_3HOUR, false);
		mTrophyLockFlag[8] = pref.getBoolean(Constants.TROPHY_GAME_PLAY_5HOUR, false);
		
		mTrophyImages = new int[9];
		mTrophyImages[0] = R.drawable.trophy_score_5000;
		mTrophyImages[1] = R.drawable.trophy_score_10000;
		mTrophyImages[2] = R.drawable.trophy_score_30000;
		mTrophyImages[3] = R.drawable.trophy_remove_500;
		mTrophyImages[4] = R.drawable.trophy_remove_1000;
		mTrophyImages[5] = R.drawable.trophy_remove_2000;
		mTrophyImages[6] = R.drawable.trophy_hour_1;
		mTrophyImages[7] = R.drawable.trophy_hour_3;
		mTrophyImages[8] = R.drawable.trophy_hour_5;
		
		mTrophyExplainHintImages = new int[9];
		mTrophyExplainHintImages[0] = R.drawable.trophy_hint_score_5000;
		mTrophyExplainHintImages[1] = R.drawable.trophy_hint_score_10000;
		mTrophyExplainHintImages[2] = R.drawable.trophy_hint_score_30000;
		mTrophyExplainHintImages[3] = R.drawable.trophy_hint_remove_500;
		mTrophyExplainHintImages[4] = R.drawable.trophy_hint_remove_1000;
		mTrophyExplainHintImages[5] = R.drawable.trophy_hint_remove_2000;
		mTrophyExplainHintImages[6] = R.drawable.trophy_hint_hour_1;
		mTrophyExplainHintImages[7] = R.drawable.trophy_hint_hour_3;
		mTrophyExplainHintImages[8] = R.drawable.trophy_hint_hour_5;
		
		mTrophyExplainAchieveImages = new int[9];
		mTrophyExplainAchieveImages[0] = R.drawable.trophy_achieve_score_5000;
		mTrophyExplainAchieveImages[1] = R.drawable.trophy_achieve_score_10000;
		mTrophyExplainAchieveImages[2] = R.drawable.trophy_achieve_score_30000;
		mTrophyExplainAchieveImages[3] = R.drawable.trophy_achieve_remove_500;
		mTrophyExplainAchieveImages[4] = R.drawable.trophy_achieve_remove_1000;
		mTrophyExplainAchieveImages[5] = R.drawable.trophy_achieve_remove_2000;
		mTrophyExplainAchieveImages[6] = R.drawable.trophy_achieve_hour_1;
		mTrophyExplainAchieveImages[7] = R.drawable.trophy_achieve_hour_3;
		mTrophyExplainAchieveImages[8] = R.drawable.trophy_achieve_hour_5;
				
		for (int i = 0; i < 9; i++) {
			mTrophyImageButtons[i] = (ImageButton)findViewById(this.getResources().getIdentifier(PREFIX + TROPHY + i, "id", getPackageName()));
			mTrophySltImageViews[i] = (ImageView)findViewById(this.getResources().getIdentifier(PREFIX + TROPHY + SELECT + i, "id", getPackageName()));
			
			mTrophyImageButtons[i].setOnClickListener(imageButtonEventHandler);
			
			if (mTrophyLockFlag[i]) {
				mTrophyImageButtons[i].setImageResource(mTrophyImages[i]);
			} else {
				mTrophyImageButtons[i].setImageResource(R.drawable.trophy_locked);
			}
		}
		
		mTrophyExplainView = (ImageView)findViewById(R.id.trophy_layout_trophy_explain);
		mBackBtn = (ImageButton)findViewById(R.id.trophy_layout_back_btn);
		mBackBtn.setOnClickListener(imageButtonEventHandler);
		
		setTrophyExplainText();
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		Utils.recursiveRecycle(getWindow().getDecorView());
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
	}
	
	private void setTrophyExplainText() {
		if (mTrophyLockFlag[mCurrentSelectedIndex]) {
			mTrophyExplainView.setImageResource(mTrophyExplainAchieveImages[mCurrentSelectedIndex]);
		} else {
			mTrophyExplainView.setImageResource(mTrophyExplainHintImages[mCurrentSelectedIndex]);
		}
	}
	
	private class ImageButtonEventHandler implements View.OnClickListener {
		public void onClick(View v) {
			int id = v.getId();
			if (id == R.id.trophy_layout_trophy_0) {
				mTrophySltImageViews[mCurrentSelectedIndex].setVisibility(View.GONE);
				mCurrentSelectedIndex = 0;
				mTrophySltImageViews[mCurrentSelectedIndex].setVisibility(View.VISIBLE);
				setTrophyExplainText();
			} else if (id == R.id.trophy_layout_trophy_1) {
				mTrophySltImageViews[mCurrentSelectedIndex].setVisibility(View.GONE);
				mCurrentSelectedIndex = 1;
				mTrophySltImageViews[mCurrentSelectedIndex].setVisibility(View.VISIBLE);
				setTrophyExplainText();
			} else if (id == R.id.trophy_layout_trophy_2) {
				mTrophySltImageViews[mCurrentSelectedIndex].setVisibility(View.GONE);
				mCurrentSelectedIndex = 2;
				mTrophySltImageViews[mCurrentSelectedIndex].setVisibility(View.VISIBLE);
				setTrophyExplainText();
			} else if (id == R.id.trophy_layout_trophy_3) {
				mTrophySltImageViews[mCurrentSelectedIndex].setVisibility(View.GONE);
				mCurrentSelectedIndex = 3;
				mTrophySltImageViews[mCurrentSelectedIndex].setVisibility(View.VISIBLE);
				setTrophyExplainText();
			} else if (id == R.id.trophy_layout_trophy_4) {
				mTrophySltImageViews[mCurrentSelectedIndex].setVisibility(View.GONE);
				mCurrentSelectedIndex = 4;
				mTrophySltImageViews[mCurrentSelectedIndex].setVisibility(View.VISIBLE);
				setTrophyExplainText();
			} else if (id == R.id.trophy_layout_trophy_5) {
				mTrophySltImageViews[mCurrentSelectedIndex].setVisibility(View.GONE);
				mCurrentSelectedIndex = 5;
				mTrophySltImageViews[mCurrentSelectedIndex].setVisibility(View.VISIBLE);
				setTrophyExplainText();
			} else if (id == R.id.trophy_layout_trophy_6) {
				mTrophySltImageViews[mCurrentSelectedIndex].setVisibility(View.GONE);
				mCurrentSelectedIndex = 6;
				mTrophySltImageViews[mCurrentSelectedIndex].setVisibility(View.VISIBLE);
				setTrophyExplainText();
			} else if (id == R.id.trophy_layout_trophy_7) {
				mTrophySltImageViews[mCurrentSelectedIndex].setVisibility(View.GONE);
				mCurrentSelectedIndex = 7;
				mTrophySltImageViews[mCurrentSelectedIndex].setVisibility(View.VISIBLE);
				setTrophyExplainText();
			} else if (id == R.id.trophy_layout_trophy_8) {
				mTrophySltImageViews[mCurrentSelectedIndex].setVisibility(View.GONE);
				mCurrentSelectedIndex = 8;
				mTrophySltImageViews[mCurrentSelectedIndex].setVisibility(View.VISIBLE);
				setTrophyExplainText();
			} else if (id == R.id.trophy_layout_back_btn) {
				startActivity(new Intent(TrophyListActivity.this, MainActivity.class));
				finish();
			}
		}
	}
}
