package team.madcat.bubblepang;

import team.madcat.audio.GameAudioPlayer;
import team.madcat.constants.Constants;
import team.madcat.game.GameActivity;
import team.madcat.help.HelpActivity;
import team.madcat.trophy.TrophyListActivity;
import team.madcat.utils.Utils;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;

public class MainActivity extends Activity {
	private Context mContext = null;
	
	private ImageView mStartGameBtn = null, mTrophyBtn = null, mHelpBtn = null;
	private CheckBox mVibrateCheckBox = null, mBgmCheckBox = null, mSoundCheckBox = null; 
	
	private SharedPreferences mPref = null;
	private GameAudioPlayer mGameAudioPlayer = null;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_layout_main);
        
        mContext = this;
        
        mStartGameBtn = (ImageView)findViewById(R.id.main_layout_start_btn);
        mTrophyBtn = (ImageView)findViewById(R.id.main_layout_trophy_btn);
        mHelpBtn = (ImageView)findViewById(R.id.main_layout_help_btn);
        mVibrateCheckBox = (CheckBox)findViewById(R.id.main_layout_checkbox_vib);
        mBgmCheckBox = (CheckBox)findViewById(R.id.main_layout_checkbox_bgm);
        mSoundCheckBox = (CheckBox)findViewById(R.id.main_layout_checkbox_sound);
        mGameAudioPlayer = new GameAudioPlayer(this);
        
        ButtonEventHandler buttonEventHandler = new ButtonEventHandler();
        mStartGameBtn.setOnClickListener(buttonEventHandler);
        mTrophyBtn.setOnClickListener(buttonEventHandler);
        mHelpBtn.setOnClickListener(buttonEventHandler);
        
        CheckBoxEventHandler checkBoxEventHandler = new CheckBoxEventHandler();
        mVibrateCheckBox.setOnCheckedChangeListener(checkBoxEventHandler);
        mBgmCheckBox.setOnCheckedChangeListener(checkBoxEventHandler);
        mSoundCheckBox.setOnCheckedChangeListener(checkBoxEventHandler);
        
        mPref = getSharedPreferences(Constants.PREFERENCE_NAME, Context.MODE_PRIVATE);
        
        //initialize sharedpreference's properties.
        if (mPref.getBoolean(Constants.FIRST_EXECUTE, true)) {
        	SharedPreferences.Editor editor = mPref.edit();
        	editor.putBoolean(Constants.FIRST_EXECUTE, false);
        	editor.putBoolean(Constants.VIBRATION_FLAG, true);
        	editor.putBoolean(Constants.BGM_FLAG, true);
        	editor.putBoolean(Constants.SOUND_FLAG, true);
        	
        	editor.putInt(Constants.RECORD_PLAY_TIME, 0);
        	editor.putInt(Constants.RECORD_BEST_SCORE, 0);
        	editor.putInt(Constants.RECORD_BLOCK_BREAK, 0);
        	
        	editor.putBoolean(Constants.TROPHY_GAME_PLAY_1HOUR, false);
        	editor.putBoolean(Constants.TROPHY_GAME_PLAY_3HOUR, false);
        	editor.putBoolean(Constants.TROPHY_GAME_PLAY_5HOUR, false);
        	
        	editor.putBoolean(Constants.TROPHY_GAME_SCORE_5000, false);
        	editor.putBoolean(Constants.TROPHY_GAME_SCORE_10000, false);
        	editor.putBoolean(Constants.TROPHY_GAME_SCORE_30000, false);
        	
        	editor.putBoolean(Constants.TROPHY_REMOVE_NORMAL_500, false);
        	editor.putBoolean(Constants.TROPHY_REMOVE_NORMAL_1000, false);
        	editor.putBoolean(Constants.TROPHY_REMOVE_NORMAL_2000, false);
        	
        	//For test
//        	editor.putBoolean(Constants.VIBRATION_FLAG, false);
//        	editor.putBoolean(Constants.BGM_FLAG, false);
//        	editor.putBoolean(Constants.SOUND_FLAG, false);
        	
        	editor.putInt(Constants.RECORD_BEST_SCORE, 0);
        	editor.commit();
        	
        	mVibrateCheckBox.setChecked(true);
        	mBgmCheckBox.setChecked(true);
        	mSoundCheckBox.setChecked(true);

        	//For Test
//        	mVibrateCheckBox.setChecked(false);
//        	mBgmCheckBox.setChecked(false);
//        	mSoundCheckBox.setChecked(false);
        } else {
        	mVibrateCheckBox.setChecked(mPref.getBoolean(Constants.VIBRATION_FLAG, true));
            mBgmCheckBox.setChecked(mPref.getBoolean(Constants.BGM_FLAG, true));
            mSoundCheckBox.setChecked(mPref.getBoolean(Constants.SOUND_FLAG, true));
        }
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (mBgmCheckBox.isChecked()) {
			mGameAudioPlayer.startBGM(R.raw.main_bgm, true);
		}
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		mGameAudioPlayer.pauseBGM();
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		mGameAudioPlayer.stopBGM();
		Utils.recursiveRecycle(getWindow().getDecorView());
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
	}
	
	private class ButtonEventHandler implements View.OnClickListener {
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Intent intent = null;
			
			if (mSoundCheckBox.isChecked()) {
				new ButtonSoundPlayAsyncTask().execute();
				SystemClock.sleep(200);
			}
			
			int id = v.getId();
			if (id == R.id.main_layout_start_btn) {
				intent = new Intent(MainActivity.this, GameActivity.class);
			} else if (id == R.id.main_layout_trophy_btn) {
				intent = new Intent(MainActivity.this, TrophyListActivity.class);
			} else if (id == R.id.main_layout_help_btn) {
				intent = new Intent(MainActivity.this, HelpActivity.class);
			}
			
			if (intent != null) {
				startActivity(intent);
				finish();
			}
		}
	}
	
	private class CheckBoxEventHandler implements CompoundButton.OnCheckedChangeListener {
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			// TODO Auto-generated method stub
			Editor prefEditor = mPref.edit();
			
			int id = buttonView.getId();
			if (id == R.id.main_layout_checkbox_vib) {
				prefEditor.putBoolean(Constants.VIBRATION_FLAG, isChecked);
				prefEditor.commit();
			} else if (id == R.id.main_layout_checkbox_bgm) {
				if (isChecked) {
					mGameAudioPlayer.startBGM(R.raw.main_bgm, true);
				} else {
					mGameAudioPlayer.stopBGM();
				}
				prefEditor.putBoolean(Constants.BGM_FLAG, isChecked);
				prefEditor.commit();
			} else if (id == R.id.main_layout_checkbox_sound) {
				prefEditor.putBoolean(Constants.SOUND_FLAG, isChecked);
				prefEditor.commit();
			}
		}
	}
	
	private class ButtonSoundPlayAsyncTask extends AsyncTask<Void, Void, Void> {
		GameAudioPlayer buttonSoundPlayer = null;
		
		public ButtonSoundPlayAsyncTask() {
			// TODO Auto-generated constructor stub
			buttonSoundPlayer = new GameAudioPlayer(mContext);
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			buttonSoundPlayer.startBGM(R.raw.main_button_touch, false);
			SystemClock.sleep(1000);
			buttonSoundPlayer.stopBGM();
			
			return null;
		}
	}
}